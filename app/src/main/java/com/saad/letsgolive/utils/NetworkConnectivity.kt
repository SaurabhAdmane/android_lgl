package com.saad.letsgolive.utils

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Build
import android.support.annotation.RequiresApi

/**
 * Created by FAYEGU-CONT on 2/12/2018.
 */
object NetworkConnectivity {

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun isNetworkAvailable(context: Context): Boolean {

        try {
            val connectivity = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                val networks = connectivity.allNetworks
                var networkInfo: NetworkInfo
                for (mNetwork in networks) {
                    networkInfo = connectivity.getNetworkInfo(mNetwork)
                    if (networkInfo.state == NetworkInfo.State.CONNECTED) {
                        return true
                    }
                }
            } else {
                if (true) {
                    val availableNetworks: Array<NetworkInfo>? = connectivity.allNetworkInfo
                    if (availableNetworks != null)
                        for (i in availableNetworks.indices)
                            if (availableNetworks[i].state == NetworkInfo.State.CONNECTED) {
                                return true
                            }

                }
            }
            return false
        } catch (e: NullPointerException) {
            return false
        }

    }
}
