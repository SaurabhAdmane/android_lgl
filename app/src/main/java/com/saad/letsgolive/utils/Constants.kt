package com.saad.letsgolive.utils

/**
 * Created by FAYEGU-CONT on 6/26/2018.
 */
object Constants {

    const val PREFERENCE_NAME_KEY = "LGL_USER_DATA"
    const val NO_NETWORK_ACTION = "no_network_action"
    const val SCREEN_FEATURE = "screen_feature"
    const val CONNECTIVITY_ACTION = "CONNECTIVITY_ACTION"
    const val USER_PROFILE_PIC_URL = "user_profile_pic"
    const val SHARED_LAST_NAME = "last_name"
    const val SHARED_FIRST_NAME = "first_name"
    const val SHARED_ID = "id"
    const val SHARED_EMAIL = "email"
    const val SHARED_MOBILE = "mobile"
    const val SHARED_DOB = "dob"
    const val SHARED_IS_ACTIVE = "is_active"
    const val SHARED_ROLE = "role"
    const val SHARED_CREATED_AT = "created_at"
    const val SHARED_DEVICE_ID = "device_id"
    const val SHARED_UDID = "udid"
    const val SHARED_AUTH_TOKEN = "auth_token"
    const val SHARED_IS_NOTIFICATION = "is_notification"
    const val SHARED_UPDATED_AT = "updated_at"
}