package com.saad.letsgolive.utils

import android.app.Service
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.app.ActivityOptionsCompat
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.app.AppCompatActivity
import android.support.v7.app.AppCompatDialog
import android.util.Log
import android.view.Gravity
import android.view.View
import android.view.Window
import android.widget.Toast
import com.saad.letsgolive.LGLApp
import com.saad.letsgolive.R
import com.saad.letsgolive.models.signup.SignupResponseBean
import com.saad.letsgolive.utils.Constants.NO_NETWORK_ACTION
import com.twitter.sdk.android.core.DefaultLogger
import com.twitter.sdk.android.core.Twitter
import com.twitter.sdk.android.core.TwitterAuthConfig
import com.twitter.sdk.android.core.TwitterConfig


/**
 * Created by FAYEGU-CONT on 11/30/2017.
 */
object CommonUtils {

    /*
    Function to show toast message
     */
    fun showToastMsg(message: String) {
        val toast = Toast.makeText(LGLApp.context, message, Toast.LENGTH_LONG)
        toast.setGravity(Gravity.CENTER, 0, 4)
        toast.show()
    }

    /*
    Function to show progress dialog
     */
    fun showAppCompactDialog(context: Context): AppCompatDialog {
        val appCompatDialog = AppCompatDialog(context)
        try {
            appCompatDialog.supportRequestWindowFeature(Window.FEATURE_NO_TITLE)
            appCompatDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            appCompatDialog.setContentView(R.layout.custom_progressbar)
            appCompatDialog.setCancelable(false)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return appCompatDialog
    }

    fun canConnect(mContext: Context): Boolean {
        var ret = false
        val connectMgr = mContext.getSystemService(Service.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkinfo = connectMgr.activeNetworkInfo

        if (networkinfo != null && networkinfo.isConnected && networkinfo.state == NetworkInfo.State.CONNECTED)
            ret = true
        else {
            showToastMsg(mContext.resources.getString(R.string.connecttointernet))
            LocalBroadcastManager.getInstance(mContext).sendBroadcast(Intent(NO_NETWORK_ACTION))
        }
        Log.v("component coreutils", "can connect : $ret")

        return ret
    }

    /*
   Function to configure Twitter SDK
    */
    fun configureTwitterSDK() {
        val authConfig = TwitterAuthConfig(
                LGLApp.context.getString(R.string.twitter_consumer_key_mine),
                LGLApp.context.getString(R.string.twitter_consumer_secret_mine))

        val twitterConfig = TwitterConfig.Builder(LGLApp.context)
                .twitterAuthConfig(authConfig)
                .logger(DefaultLogger(Log.DEBUG))
                .debug(true)
                .build()

        Twitter.initialize(twitterConfig)
    }

    fun navigateTo(activity: Context, bundle: Bundle, keyName: String, view: View?, toClass: AppCompatActivity) {
        val intent = Intent(activity, toClass::class.java)
        intent.putExtra(keyName, if (null != bundle) bundle else Bundle())
        if (null != view) {
            val optionsCompat = ActivityOptionsCompat.makeSceneTransitionAnimation(activity as AppCompatActivity,
                    view, LGLApp.context.getString(R.string.str_appcompact_transaction))
            ActivityCompat.startActivity(activity, intent, optionsCompat.toBundle())
        } else {
            ActivityCompat.startActivity(activity, intent, null)
        }
    }

    fun saveUserDatta(response: SignupResponseBean) {
        LGLSharedPreference.insertString(Constants.SHARED_LAST_NAME, response.user.lastName)
        LGLSharedPreference.insertString(Constants.SHARED_FIRST_NAME, response.user.firstName)
        LGLSharedPreference.insertString(Constants.SHARED_ID, response.user.id)
        LGLSharedPreference.insertString(Constants.SHARED_EMAIL, response.user.email)
        LGLSharedPreference.insertString(Constants.SHARED_MOBILE, response.user.mobile)
        LGLSharedPreference.insertString(Constants.SHARED_DOB, response.user.dob)
        LGLSharedPreference.insertString(Constants.SHARED_IS_ACTIVE, response.user.isActive)
        LGLSharedPreference.insertString(Constants.SHARED_ROLE, response.user.role)
        LGLSharedPreference.insertString(Constants.SHARED_AUTH_TOKEN, response.user.authToken)
        LGLSharedPreference.insertString(Constants.SHARED_IS_NOTIFICATION, response.user.isNotification)
    }
}