package com.saad.letsgolive.utils

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.support.v4.app.ActivityCompat.requestPermissions
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import com.saad.letsgolive.LGLApp.Companion.context
import java.util.*

/**
 * Created by FAYEGU-CONT on 6/26/2018.
 */
class PermissionHandler(context: Context) {

    companion object {

        val REQUEST_MULTIPLE_PERMISSIONS = 2000
        val READ_CONTACTS_PERMISSION = 1
        val CAMERA_PERMISSION = 2
        val WRITE_STORAGE_PERMISSION = 3
        val READ_STORAGE_PERMISSION = 4
        val READ_SMS_PERMISSION = 5
        val READ_PHONE_STATE = 6
    }

    fun checkPermission(permissions: Array<String>, isActivity: Boolean, fragment: Fragment): Boolean {
        val prmsnsToBeGranted = ArrayList<String>()
        for (i in permissions.indices) {
            val result = ContextCompat.checkSelfPermission(context, permissions[i])
            if (result != PackageManager.PERMISSION_GRANTED)
                prmsnsToBeGranted.add(permissions[i])
        }

        if (prmsnsToBeGranted.size > 0) {
            requestPermission(prmsnsToBeGranted.toTypedArray(), isActivity, fragment)
            return false
        } else {
            return true
        }
    }

    //for non fragment classes
    fun isPermissionGranted(permission: String): Boolean {
        val result = ContextCompat.checkSelfPermission(context, permission)
        return result == PackageManager.PERMISSION_GRANTED
    }

    fun requestPermission(permissions: Array<String>, isActivity: Boolean, fragment: Fragment) {
        if (permissions.size > 1) {
            fragment.requestPermissions(permissions, REQUEST_MULTIPLE_PERMISSIONS)
        } else {
            when (permissions[0]) {
                Manifest.permission.READ_CONTACTS -> if (isActivity) {
                    requestPermissions(context as Activity, permissions, READ_CONTACTS_PERMISSION)
                } else {
                    fragment.requestPermissions(arrayOf(Manifest.permission.READ_CONTACTS), READ_CONTACTS_PERMISSION)
                }

                Manifest.permission.CAMERA -> if (isActivity) {
                    requestPermissions(context as Activity, permissions, CAMERA_PERMISSION)
                } else {
                    fragment.requestPermissions(arrayOf(Manifest.permission.CAMERA), CAMERA_PERMISSION)
                }

                Manifest.permission.READ_EXTERNAL_STORAGE -> if (isActivity) {
                    requestPermissions(context as Activity, permissions, READ_STORAGE_PERMISSION)
                } else {
                    fragment.requestPermissions(arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE), READ_STORAGE_PERMISSION)
                }

                Manifest.permission.WRITE_EXTERNAL_STORAGE -> if (isActivity) {
                    requestPermissions(context as Activity, permissions, WRITE_STORAGE_PERMISSION)
                } else {
                    fragment.requestPermissions(arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), WRITE_STORAGE_PERMISSION)
                }

                Manifest.permission.READ_SMS -> if (isActivity) {
                    requestPermissions(context as Activity, permissions, READ_SMS_PERMISSION)
                } else {
                    fragment.requestPermissions(arrayOf(Manifest.permission.READ_SMS), READ_SMS_PERMISSION)
                }

                Manifest.permission.READ_PHONE_STATE -> if (isActivity) {
                    requestPermissions(context as Activity, permissions, READ_PHONE_STATE)
                } else {
                    fragment.requestPermissions(arrayOf(Manifest.permission.READ_PHONE_STATE), READ_PHONE_STATE)
                }

                else -> {
                }
            }
        }
    }
}