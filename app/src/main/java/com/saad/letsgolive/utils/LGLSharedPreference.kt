package com.saad.letsgolive.utils

import android.content.Context
import android.content.SharedPreferences
import com.google.gson.Gson
import com.saad.letsgolive.LGLApp


/**
 * Created by FAYEGU-CONT on 6/26/2018.
 */
object LGLSharedPreference {

    val preferences: SharedPreferences
        @Synchronized get() =
            LGLApp.context.getSharedPreferences(Constants.PREFERENCE_NAME_KEY, Context.MODE_PRIVATE)

    @Synchronized
    fun insertString(key: String, value: String) {
        preferences.edit().putString(key, value).apply()
    }

    @Synchronized
    fun insertBoolean(key: String, value: Boolean) {
        preferences.edit().putBoolean(key, value).apply()
    }

    @Synchronized
    fun remove(key: String) {
        preferences.edit().remove(key).apply()
    }

    @Synchronized
    fun clear() {
        preferences.edit().clear().apply()
    }

    @Synchronized
    fun getString(key: String): String? {
        return preferences.getString(key, "")
    }

    fun getBoolean(key: String): Boolean {
        return preferences.getBoolean(key, false)
    }

    @Synchronized
    fun insertObject(key: String, model: Any) {
        preferences.edit().putString(key, Gson().toJson(model)).apply()
    }

    @Synchronized
    fun getObject(key: String, modelClass: Class<*>): Any {
        return Gson().fromJson(preferences.getString(key, null), modelClass)
    }

    @Synchronized
    fun contain(key: String): Boolean {
        return preferences.contains(key)
    }

    @Synchronized
    fun insertLong(key: String, value: Long) {
        preferences.edit().putLong(key, value).apply()
    }

    @Synchronized
    fun getLong(key: String): Long {
        return preferences.getLong(key, 0)
    }

}
/*
class LGLSharedPreference {

    val tmwSharedPreference: SharedPreferences
        get() = LGLApp.context.getSharedPreferences(SHARED_PREF, Context.MODE_PRIVATE)

    fun clearAllData(sharedPreferences: SharedPreferences) {
        sharedPreferences.edit().clear().apply()
    }

    fun clearAllOldData() {
        try {
            clearAllData(LGLApp.context.getSharedPreferences("UserInfo", Context.MODE_PRIVATE))
            clearAllData(LGLApp.context.getSharedPreferences("Device", Context.MODE_PRIVATE))
            clearAllData(PreferenceManager.getDefaultSharedPreferences(LGLApp.context))
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    fun setUsername(accountName: String) {
        val editor = tmwSharedPreference.edit()
        editor.putString(KEY_ACCOUNT_NAME, accountName)
        editor.apply()
    }

    fun setAuthToken(authToken: String) {
        val editor = tmwSharedPreference.edit()
        editor.putString(KEY_AUTH_TOKEN, authToken)
        editor.apply()
    }

    companion object {

        private val TAG = LGLSharedPreference::class.java.simpleName
        private val KEY_ACCOUNT_NAME = "account_name"
        private val KEY_AUTH_TOKEN = "auth_token"
        private val SHARED_PREF = "LGL_SHAREDPREF"

        fun setStringValue(sharedPreferences: SharedPreferences, key: String, value: String) {
            try {
                val editor = sharedPreferences.edit()
                editor.putString(key, value)
                editor.apply()
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }

        fun getStringValue(sharedPreferences: SharedPreferences, key: String): String {
            try {
                return sharedPreferences.getString(key, "")
            } catch (e: Exception) {
                return ""
            }

        }

        fun setIntValue(sharedPreferences: SharedPreferences, key: String, value: Int) {
            val editor = sharedPreferences.edit()
            editor.putInt(key, value)
            editor.apply()
        }

        fun getIntValue(sharedPreferences: SharedPreferences, key: String): Int {
            try {
                return sharedPreferences.getInt(key, -1)
            } catch (e: Exception) {
                e.printStackTrace()
                return 0
            }

        }

        fun setBooleanValue(sharedPreferences: SharedPreferences, key: String, value: Boolean) {
            val editor = sharedPreferences.edit()
            editor.putBoolean(key, value)
            editor.apply()
        }

        fun getBooleanValue(sharedPreferences: SharedPreferences, key: String): Boolean {
            try {
                return sharedPreferences.getBoolean(key, false)
            } catch (e: Exception) {
                return false
            }

        }

        //    public static String getFCMToken() {
        //        LGLSharedPreference tmwSharedPreference = new LGLSharedPreference();
        //        String refreshedToken = "";
        //        refreshedToken = getStringValue(tmwSharedPreference.getTMWSharedPreference(), FCM_TOKEN);
        //        if (refreshedToken.length() == 0) {
        //            refreshedToken = FirebaseInstanceId.getInstance().getToken();
        //            setStringValue(tmwSharedPreference.getTMWSharedPreference(),
        //                    FCM_TOKEN, refreshedToken);
        //        }
        //        Log.e(TAG, "Refreshed token: " + refreshedToken);
        //        return refreshedToken;
        //    }

        fun setLongValue(sharedPreferences: SharedPreferences, key: String, value: Long) {
            val editor = sharedPreferences.edit()
            editor.putLong(key, value)
            editor.apply()
        }

        fun getLongValue(sharedPreferences: SharedPreferences, key: String): Long {
            try {
                return sharedPreferences.getLong(key, 0.0.toLong())
            } catch (e: Exception) {
                return 0
            }

        }

        fun setFloatValue(sharedPreferences: SharedPreferences, key: String, value: Float) {
            val editor = sharedPreferences.edit()
            editor.putFloat(key, value)
            editor.apply()
        }

        fun getFloatValue(sharedPreferences: SharedPreferences, key: String): Float {
            try {
                return sharedPreferences.getFloat(key, 0.0.toFloat())
            } catch (e: Exception) {
                return 0f
            }

        }

        private val instance: SharedPreferences
            get() {
                val preference = LGLSharedPreference()
                return preference.tmwSharedPreference
            }
    }
}*/
