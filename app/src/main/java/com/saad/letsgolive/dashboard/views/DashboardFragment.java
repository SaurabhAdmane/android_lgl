package com.saad.letsgolive.dashboard.views;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.saad.letsgolive.LGLApp;
import com.saad.letsgolive.R;
import com.saad.letsgolive.baseclass.BaseFragment;
import com.saad.letsgolive.customviews.CustomNavigationView;
import com.saad.letsgolive.dashboard.adapters.DashboardTabPagerAdapter;
import com.saad.letsgolive.dashboard.viewmodel.DashboardViewModel;
import com.saad.letsgolive.databinding.FragmentDashboardBinding;
import com.saad.letsgolive.profile.ProfileFragment;
import com.saad.letsgolive.signupLogin.activity.LoginActivity;
import com.saad.letsgolive.utils.CommonUtils;
import com.saad.letsgolive.utils.LGLSharedPreference;

/**
 * Created by saurabha on 6/20/2018.
 */

public class DashboardFragment extends BaseFragment implements View.OnClickListener,
        CustomNavigationView.NavigationItemSelectedListner {

    private FragmentDashboardBinding binding;
    private ActionBarDrawerToggle actionBarDrawerToggle;
    private int previousPosition = -1;
    private DashboardViewModel viewModel;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_dashboard, container, false);
        viewModel = new DashboardViewModel();
        binding.setViewModel(viewModel);
        init();
        setListners();
        setUpNavigationView();
        setViewPager();

        wrapTabIndicatorToTitle(binding.tabLayoutOffers, 40, 40);

        return binding.getRoot();
    }

    private void setViewPager() {
        binding.tabLayoutOffers.setTabMode(TabLayout.MODE_FIXED);
        DashboardTabPagerAdapter dashboardTabPagerAdapter = new DashboardTabPagerAdapter(getActivity().getSupportFragmentManager(), viewModel.getDashboardTab());
        binding.viewPagerDashboard.setAdapter(dashboardTabPagerAdapter);
        binding.tabLayoutOffers.setupWithViewPager(binding.viewPagerDashboard);
    }

//    private void changeTabsFont() {
//        ViewGroup childTabLayout = (ViewGroup) tabLayout.getChildAt(0);
//        for (int i = 0; i < childTabLayout.getChildCount(); i++) {
//            ViewGroup viewTab = (ViewGroup) childTabLayout.getChildAt(i);
//            for (int j = 0; j < viewTab.getChildCount(); j++) {
//                View tabTextView = viewTab.getChildAt(j);
//                if (tabTextView instanceof TextView) {
//                    Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/gotham-ultra.ttf");
//                    ((TextView) tabTextView).setTypeface(typeface);
//                    ((TextView) tabTextView).setTextSize(TypedValue.COMPLEX_UNIT_DIP, getResources().getDimension(R.dimen.text_small));
//                }
//            }
//        }
//    }

    public void wrapTabIndicatorToTitle(TabLayout tabLayout, int externalMargin, int internalMargin) {
        View tabStrip = tabLayout.getChildAt(0);
        Typeface tf = Typeface.createFromAsset(getActivity().getAssets(), "bauhaus medium bt.ttf");
        if (tabStrip instanceof ViewGroup) {
            ViewGroup tabStripGroup = (ViewGroup) tabStrip;
            int childCount = ((ViewGroup) tabStrip).getChildCount();
            for (int i = 0; i < childCount; i++) {
                View tabView = tabStripGroup.getChildAt(i);
                //set minimum width to 0 for instead for small texts, indicator is not wrapped as expected
                if (tabView instanceof TextView) {
                    ((TextView) tabView).setTypeface(tf);
                }
                tabView.setMinimumWidth(0);
                // set padding to 0 for wrapping indicator as title
                tabView.setPadding(0, tabView.getPaddingTop(), 0, tabView.getPaddingBottom());
                // setting custom margin between tabs
                if (tabView.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
                    ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) tabView.getLayoutParams();
                    if (i == 0) {
                        // left
                        settingMargin(layoutParams, externalMargin, internalMargin);
                    } else if (i == childCount - 1) {
                        // right
                        settingMargin(layoutParams, internalMargin, externalMargin);
                    } else {
                        // internal
                        settingMargin(layoutParams, internalMargin, internalMargin);
                    }
                }
            }
            tabLayout.requestLayout();
        }
    }

    private void settingMargin(ViewGroup.MarginLayoutParams layoutParams, int start, int end) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            layoutParams.setMarginStart(start);
            layoutParams.setMarginEnd(end);
        } else {
            layoutParams.leftMargin = start;
            layoutParams.rightMargin = end;
        }
    }

    private void setListners() {
        binding.imgDrawer.setOnClickListener(this);
        binding.imgCrown.setOnClickListener(this);
        binding.includeDrawer.relative1.setOnClickListener(this);
        binding.includeDrawer.relative2.setOnClickListener(this);
        binding.includeDrawer.relative3.setOnClickListener(this);
        binding.includeDrawer.relative4.setOnClickListener(this);
        binding.includeDrawer.relative5.setOnClickListener(this);
        binding.includeDrawer.relative6.setOnClickListener(this);
    }

    private void init() {
        binding.bottomNavigation1.menuProfile.setOnClickListener(this);
        binding.bottomNavigation1.menuMsg.setOnClickListener(this);
        binding.bottomNavigation1.menuFavorite.setOnClickListener(this);
        binding.bottomNavigation1.menuHome.setOnClickListener(this);
    }

    private void setUpNavigationView() {
        binding.drawerLayout.addDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                binding.drawerLayout.getChildAt(0).setTranslationX(slideOffset * drawerView.getWidth());
                binding.drawerLayout.bringChildToFront(drawerView);
                binding.drawerLayout.requestLayout();
            }

            @Override
            public void onDrawerOpened(View drawerView) {

            }

            @Override
            public void onDrawerClosed(View drawerView) {

            }

            @Override
            public void onDrawerStateChanged(int newState) {

            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.menu_favorite:

                break;

            case R.id.menu_home:
//                replaceFragment(R.id.fragment_cointner, new ProfileFragment(), "Profile", "profile");
                break;

            case R.id.menu_msg:

                break;

            case R.id.menu_profile:
                replaceFragment(R.id.fragment_cointner, new ProfileFragment(), "Profile", "profile");
                break;

            case R.id.img_crown:
                CommonUtils.INSTANCE.showToastMsg("Crown Clicked");
                break;

            case R.id.img_drawer:
                binding.drawerLayout.openDrawer(Gravity.START);
                break;

            case R.id.relative_1:
                binding.includeDrawer.relative1.setBackgroundColor(ContextCompat.getColor(LGLApp.Companion.getContext(), R.color.colorAccent));
                binding.includeDrawer.relative2.setBackgroundColor(ContextCompat.getColor(LGLApp.Companion.getContext(), android.R.color.transparent));
                binding.includeDrawer.relative3.setBackgroundColor(ContextCompat.getColor(LGLApp.Companion.getContext(), android.R.color.transparent));
                binding.includeDrawer.relative4.setBackgroundColor(ContextCompat.getColor(LGLApp.Companion.getContext(), android.R.color.transparent));
                binding.includeDrawer.relative5.setBackgroundColor(ContextCompat.getColor(LGLApp.Companion.getContext(), android.R.color.transparent));
                binding.includeDrawer.relative6.setBackgroundColor(ContextCompat.getColor(LGLApp.Companion.getContext(), android.R.color.transparent));
                binding.drawerLayout.closeDrawers();
                break;

            case R.id.relative_2:
                binding.includeDrawer.relative1.setBackgroundColor(ContextCompat.getColor(LGLApp.Companion.getContext(), android.R.color.transparent));
                binding.includeDrawer.relative2.setBackgroundColor(ContextCompat.getColor(LGLApp.Companion.getContext(), R.color.colorAccent));
                binding.includeDrawer.relative3.setBackgroundColor(ContextCompat.getColor(LGLApp.Companion.getContext(), android.R.color.transparent));
                binding.includeDrawer.relative4.setBackgroundColor(ContextCompat.getColor(LGLApp.Companion.getContext(), android.R.color.transparent));
                binding.includeDrawer.relative5.setBackgroundColor(ContextCompat.getColor(LGLApp.Companion.getContext(), android.R.color.transparent));
                binding.includeDrawer.relative6.setBackgroundColor(ContextCompat.getColor(LGLApp.Companion.getContext(), android.R.color.transparent));
                binding.drawerLayout.closeDrawers();
                break;

            case R.id.relative_3:
                binding.includeDrawer.relative1.setBackgroundColor(ContextCompat.getColor(LGLApp.Companion.getContext(), android.R.color.transparent));
                binding.includeDrawer.relative2.setBackgroundColor(ContextCompat.getColor(LGLApp.Companion.getContext(), android.R.color.transparent));
                binding.includeDrawer.relative3.setBackgroundColor(ContextCompat.getColor(LGLApp.Companion.getContext(), R.color.colorAccent));
                binding.includeDrawer.relative4.setBackgroundColor(ContextCompat.getColor(LGLApp.Companion.getContext(), android.R.color.transparent));
                binding.includeDrawer.relative5.setBackgroundColor(ContextCompat.getColor(LGLApp.Companion.getContext(), android.R.color.transparent));
                binding.includeDrawer.relative6.setBackgroundColor(ContextCompat.getColor(LGLApp.Companion.getContext(), android.R.color.transparent));
                binding.drawerLayout.closeDrawers();
                break;

            case R.id.relative_4:
                binding.includeDrawer.relative1.setBackgroundColor(ContextCompat.getColor(LGLApp.Companion.getContext(), android.R.color.transparent));
                binding.includeDrawer.relative2.setBackgroundColor(ContextCompat.getColor(LGLApp.Companion.getContext(), android.R.color.transparent));
                binding.includeDrawer.relative3.setBackgroundColor(ContextCompat.getColor(LGLApp.Companion.getContext(), android.R.color.transparent));
                binding.includeDrawer.relative4.setBackgroundColor(ContextCompat.getColor(LGLApp.Companion.getContext(), R.color.colorAccent));
                binding.includeDrawer.relative5.setBackgroundColor(ContextCompat.getColor(LGLApp.Companion.getContext(), android.R.color.transparent));
                binding.includeDrawer.relative6.setBackgroundColor(ContextCompat.getColor(LGLApp.Companion.getContext(), android.R.color.transparent));
                binding.drawerLayout.closeDrawers();
                break;

            case R.id.relative_5:
                binding.includeDrawer.relative1.setBackgroundColor(ContextCompat.getColor(LGLApp.Companion.getContext(), android.R.color.transparent));
                binding.includeDrawer.relative2.setBackgroundColor(ContextCompat.getColor(LGLApp.Companion.getContext(), android.R.color.transparent));
                binding.includeDrawer.relative3.setBackgroundColor(ContextCompat.getColor(LGLApp.Companion.getContext(), android.R.color.transparent));
                binding.includeDrawer.relative4.setBackgroundColor(ContextCompat.getColor(LGLApp.Companion.getContext(), android.R.color.transparent));
                binding.includeDrawer.relative5.setBackgroundColor(ContextCompat.getColor(LGLApp.Companion.getContext(), R.color.colorAccent));
                binding.includeDrawer.relative6.setBackgroundColor(ContextCompat.getColor(LGLApp.Companion.getContext(), android.R.color.transparent));
                binding.drawerLayout.closeDrawers();
                break;

            case R.id.relative_6:
                binding.includeDrawer.relative1.setBackgroundColor(ContextCompat.getColor(LGLApp.Companion.getContext(), android.R.color.transparent));
                binding.includeDrawer.relative2.setBackgroundColor(ContextCompat.getColor(LGLApp.Companion.getContext(), android.R.color.transparent));
                binding.includeDrawer.relative3.setBackgroundColor(ContextCompat.getColor(LGLApp.Companion.getContext(), android.R.color.transparent));
                binding.includeDrawer.relative4.setBackgroundColor(ContextCompat.getColor(LGLApp.Companion.getContext(), android.R.color.transparent));
                binding.includeDrawer.relative5.setBackgroundColor(ContextCompat.getColor(LGLApp.Companion.getContext(), android.R.color.transparent));
                binding.includeDrawer.relative6.setBackgroundColor(ContextCompat.getColor(LGLApp.Companion.getContext(), R.color.colorAccent));
                binding.drawerLayout.closeDrawers();
                LGLSharedPreference.INSTANCE.clear();
                Intent intent = new Intent(getActivity(), LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                break;
        }
//        if (binding.drawerLayout.isDrawerVisible(binding.d))
    }

    @Override
    public void onItemSelected(View view, int position) {
        switch (view.getId()) {

        }

        binding.drawerLayout.closeDrawers();
    }
}
