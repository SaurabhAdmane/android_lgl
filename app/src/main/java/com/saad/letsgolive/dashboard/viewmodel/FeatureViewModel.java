package com.saad.letsgolive.dashboard.viewmodel;

import com.saad.letsgolive.baseclass.BaseViewModel;

import java.util.ArrayList;

/**
 * Created by Saurabh on 6/24/2018.
 */

public class FeatureViewModel extends BaseViewModel {

    public ArrayList<String> getBanners() {
        ArrayList<String> array = new ArrayList<>();
        array.add("1");
        array.add("2");
        array.add("3");
        array.add("4");
        array.add("5");
        array.add("6");
        array.add("7");
        array.add("8");
        array.add("9");
        array.add("10");
        return array;
    }

    @Override
    public void subscribe() {

    }

    @Override
    public void clear() {

    }

    @Override
    public void dispose() {

    }
}
