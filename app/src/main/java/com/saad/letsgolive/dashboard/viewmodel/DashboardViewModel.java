package com.saad.letsgolive.dashboard.viewmodel;

import android.support.v4.app.Fragment;

import com.saad.letsgolive.LGLApp;
import com.saad.letsgolive.R;
import com.saad.letsgolive.baseclass.BaseViewModel;
import com.saad.letsgolive.models.dashboard.DrawerMenuModel;
import com.saad.letsgolive.profile.ProfileFragment;

import java.util.ArrayList;

/**
 * Created by Saurabh on 6/13/2018.
 */

public class DashboardViewModel extends BaseViewModel {


    public ArrayList<Fragment> getBottomFragment() {
        ArrayList<Fragment> bottomFragment = new ArrayList<>();
        bottomFragment.add(new ProfileFragment());
//        bottomFragment.add(new ProfileFragment());
//        bottomFragment.add(new ProfileFragment());
//        bottomFragment.add(new ProfileFragment());
        return bottomFragment;
    }

    public ArrayList<String> getDashboardTab() {
        ArrayList<String> str = new ArrayList<>();
        str.add("Featured");
        str.add("New");
        str.add("Nearby");
        return str;
    }

    public ArrayList<DrawerMenuModel> getDrawerMenu() {
        ArrayList<DrawerMenuModel> menuList = new ArrayList<>();
        DrawerMenuModel drawerMenuModel_1 = new DrawerMenuModel();
        drawerMenuModel_1.setStrDrawerName(LGLApp.Companion.getContext().getString(R.string.str_drawer_gilrls));
        drawerMenuModel_1.setI_drawerIcon(R.drawable.drawer_girls);
        menuList.add(drawerMenuModel_1);

        DrawerMenuModel drawerMenuModel_2 = new DrawerMenuModel();
        drawerMenuModel_2.setStrDrawerName(LGLApp.Companion.getContext().getString(R.string.str_drawer_talent));
        drawerMenuModel_2.setI_drawerIcon(R.drawable.drawer_talent);
        menuList.add(drawerMenuModel_2);

        DrawerMenuModel drawerMenuModel_3 = new DrawerMenuModel();
        drawerMenuModel_3.setStrDrawerName(LGLApp.Companion.getContext().getString(R.string.str_drawer_video));
        drawerMenuModel_3.setI_drawerIcon(R.drawable.drawer_video);
        menuList.add(drawerMenuModel_3);

        DrawerMenuModel drawerMenuModel_4 = new DrawerMenuModel();
        drawerMenuModel_4.setStrDrawerName(LGLApp.Companion.getContext().getString(R.string.str_drawer_contact));
        drawerMenuModel_4.setI_drawerIcon(R.drawable.drawer_contact);
        menuList.add(drawerMenuModel_4);

        DrawerMenuModel drawerMenuModel_5 = new DrawerMenuModel();
        drawerMenuModel_5.setStrDrawerName(LGLApp.Companion.getContext().getString(R.string.str_drawer_help));
        drawerMenuModel_5.setI_drawerIcon(R.drawable.drawer_help);
        menuList.add(drawerMenuModel_5);

        DrawerMenuModel drawerMenuModel_6 = new DrawerMenuModel();
        drawerMenuModel_6.setStrDrawerName(LGLApp.Companion.getContext().getString(R.string.str_drawer_logout));
        drawerMenuModel_6.setI_drawerIcon(R.drawable.drawer_logout);
        menuList.add(drawerMenuModel_6);

        return menuList;
    }

    @Override
    public void subscribe() {

    }

    @Override
    public void clear() {

    }

    @Override
    public void dispose() {

    }
}
