package com.saad.letsgolive.dashboard.views;

import android.databinding.DataBindingUtil;
import android.os.Bundle;

import com.saad.letsgolive.R;
import com.saad.letsgolive.baseclass.BaseActivity;
import com.saad.letsgolive.dashboard.viewmodel.DashboardViewModel;
import com.saad.letsgolive.databinding.ActivityDashboardBinding;

import org.jetbrains.annotations.Nullable;

/**
 * Created by Saurabh on 6/21/2018.
 */

public class DashboardActivity extends BaseActivity {

    private ActivityDashboardBinding binding;
    private DashboardViewModel viewModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_dashboard);
        viewModel = new DashboardViewModel();
        binding.setViewModel(viewModel);
        setContentView(binding.getRoot());
        addFragment(R.id.continer, new DashboardFragment(), "Dashboard");
    }
}
