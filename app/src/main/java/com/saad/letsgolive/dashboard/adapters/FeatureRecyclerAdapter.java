package com.saad.letsgolive.dashboard.adapters;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.saad.letsgolive.LGLApp;
import com.saad.letsgolive.R;
import com.saad.letsgolive.databinding.ItemFeaturesBinding;

import java.util.ArrayList;

/**
 * Created by Saurabh on 6/24/2018.
 */

public class FeatureRecyclerAdapter extends RecyclerView.Adapter<FeatureRecyclerAdapter.FeatureViewHolder> {

    private LayoutInflater mInflater;
    private ArrayList<?> dataList;
    private int[] drawables = new int[10];

    public FeatureRecyclerAdapter(ArrayList<?> dataList) {
        this.dataList = dataList;
        mInflater = (LayoutInflater) LGLApp.Companion.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        drawables = new int[]{R.drawable.image_1,
                R.drawable.image_2,
                R.drawable.icon_banner,
                R.drawable.image_4,
                R.drawable.image_5,
                R.drawable.image_6,
                R.drawable.image_7,
                R.drawable.image_8,
                R.drawable.image_9, R.drawable.image_10};
    }

    @NonNull
    @Override
    public FeatureRecyclerAdapter.FeatureViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemFeaturesBinding binding = DataBindingUtil.inflate(mInflater, R.layout.item_features, parent, false);
        return new FeatureViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull FeatureRecyclerAdapter.FeatureViewHolder holder, int position) {
        holder.setData(position);
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public class FeatureViewHolder extends RecyclerView.ViewHolder {
        private ItemFeaturesBinding binding;

        private FeatureViewHolder(ItemFeaturesBinding itemView) {
            super(itemView.getRoot());
            this.binding = itemView;
        }

        private void setData(int pos) {
            binding.imgBack.setImageResource(drawables[pos]);
//            binding.txtLiveCount.setText("saf324234");
//            binding.txtName.setText("Test Name");
//            if (pos % 2 == 0) {
//                binding.imgPic.setImageResource(R.drawable.icon_diamond);
//            }
        }
    }
}
