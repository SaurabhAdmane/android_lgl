package com.saad.letsgolive.dashboard.views;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.saad.letsgolive.R;
import com.saad.letsgolive.baseclass.BaseFragment;
import com.saad.letsgolive.customviews.Banners.CommonBannerAdapter;
import com.saad.letsgolive.dashboard.adapters.FeatureRecyclerAdapter;
import com.saad.letsgolive.dashboard.viewmodel.FeatureViewModel;
import com.saad.letsgolive.databinding.FragmentFeatureBinding;

import static com.saad.letsgolive.utils.Constants.SCREEN_FEATURE;

/**
 * Created by Saurabh on 6/24/2018.
 */

public class FeatureFragment extends BaseFragment {

    private FragmentFeatureBinding binding;
    private FeatureViewModel viewModel;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_feature, container, false);
        viewModel = new FeatureViewModel();
        binding.setViewModel(viewModel);
        setBanners();
        setRecyclerView();
        return binding.getRoot();
    }

    private void setRecyclerView() {
        FeatureRecyclerAdapter featureRecyclerAdapter = new FeatureRecyclerAdapter(viewModel.getBanners());
        binding.recyclerFeature.setLayoutManager(new StaggeredGridLayoutManager(3, OrientationHelper.VERTICAL));
        binding.recyclerFeature.setItemAnimator(new DefaultItemAnimator());
        binding.recyclerFeature.setAdapter(featureRecyclerAdapter);
    }

    private void setBanners() {
        CommonBannerAdapter bannerGvAdapter = new CommonBannerAdapter(getFragmentManager(), SCREEN_FEATURE, viewModel.getBanners());
        binding.viewPagerFeatureBanners.setAdapter(bannerGvAdapter);
        binding.indicatorFeature.setViewPager(binding.viewPagerFeatureBanners);
    }
}
