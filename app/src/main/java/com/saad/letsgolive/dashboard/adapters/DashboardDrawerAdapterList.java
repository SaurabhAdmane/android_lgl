package com.saad.letsgolive.dashboard.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.saad.letsgolive.R;
import com.saad.letsgolive.models.dashboard.DrawerMenuModel;

import java.util.ArrayList;

/**
 * Created by saurabha on 6/18/2018.
 */

public class DashboardDrawerAdapterList extends ArrayAdapter<DrawerMenuModel> {

    private Context context;
    private ArrayList<DrawerMenuModel> values;

    public DashboardDrawerAdapterList(Context context, ArrayList<DrawerMenuModel> rData) {
        super(context, R.layout.item_drawer_menu, rData);
        this.context = context;
        this.values = rData;
    }

    @Override
    public int getCount() {
        return values.size();
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.item_drawer_menu, parent, false);
        TextView mTxtName = (TextView) rowView
                .findViewById(R.id.txt_menu_name);
        mTxtName.setText(values.get(position).getStrDrawerName());

        ImageView mImg = (ImageView) rowView.findViewById(R.id.img_menu_name);
        mImg.setImageResource(values.get(position).getI_drawerIcon());
        return rowView;
    }
}
