package com.saad.letsgolive.dashboard.adapters;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.saad.letsgolive.dashboard.views.FeatureFragment;

import java.util.ArrayList;

public class DashboardTabPagerAdapter extends FragmentStatePagerAdapter {

    private ArrayList<String> arrayTabs;

    public DashboardTabPagerAdapter(FragmentManager fm, ArrayList<String> arrayList) {
        super(fm);
        this.arrayTabs = arrayList;
    }

    @Override
    public Fragment getItem(int position) {
        Bundle bundle = new Bundle();
//        bundle.putParcelableArrayList(BUNDLE_OFFERS_ARRAY, arrayTabs.get(position).getOffers());
//        bundle.putString(BUNDLE_OFFERS_TAG_NAME, arrayTabs.get(position).getTagName());
//        bundle.putBoolean(BUNDLE_OFFERS_HAS_MORE, arrayTabs.get(position).getHasMoreRows());
        switch (position) {
            case 0:
                FeatureFragment featureFragment1 = new FeatureFragment();
                featureFragment1.setArguments(bundle);
                return featureFragment1;
/*
            case 1:

                break;

            case 2:

                break;*/

            default:

                return new Fragment();
        }
    }

    @Override
    public int getCount() {
        return arrayTabs.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return arrayTabs.get(position);
    }
}
