package com.saad.letsgolive.customviews.Banners;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.saad.letsgolive.R;
import com.saad.letsgolive.baseclass.BaseFragment;
import com.saad.letsgolive.databinding.FragmentBannerItemsBinding;
import com.saad.letsgolive.utils.CommonUtils;

/**
 * Created by saurabha on 31-10-2017.
 */

public class CommonBannerFragment extends BaseFragment {

    public CommonBannerFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        FragmentBannerItemsBinding binding = DataBindingUtil.inflate(inflater, R.layout.fragment_banner_items, null, false);

        binding.imgBanner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CommonUtils.INSTANCE.showToastMsg("Banner Clicked");
//                    analyticsEventsBannerClick(strFromScreen, bannersArrayBeen.getPromoName());
//                    Bundle bundle = new Bundle();
//                    bundle.putParcelable(BUNDLE_OFFERS_DETAILS, bannersArrayBeen);
//                    bundle.putString(FROM_SCREEN, strFromScreen);
//                    NavigationHelper.navigateTo(getActivity(), FragmentFactory.OFFERS_DETAILS_FRAGMENT, bundle);
            }
        });
        return binding.getRoot();
    }

//    @Override
//    public void releaseMemory() {
//        super.releaseMemory();
//        bannersArrayBeen = null;
//    }
}
