package com.saad.letsgolive.customviews.Banners;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;

/**
 * Created by saurabha on 31-10-2017.
 */

public class CommonBannerAdapter extends FragmentStatePagerAdapter {

    private ArrayList<String> brandList;
    private ArrayList<?> arrayBannerData;
    private String strImageLink, strFromScreen = "";

    public CommonBannerAdapter(FragmentManager fm, String strFromString, ArrayList<?> arrayList) {
        super(fm);
        this.arrayBannerData = arrayList;
        this.strFromScreen = strFromString;
    }

    @Override
    public Fragment getItem(int position) {
        Bundle bundle = new Bundle();
        CommonBannerFragment bannerGvFragment = new CommonBannerFragment();
        bannerGvFragment.setArguments(bundle);
        return bannerGvFragment;
    }

    @Override
    public int getCount() {
        return arrayBannerData.size();
    }
}
