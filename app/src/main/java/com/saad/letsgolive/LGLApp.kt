package com.saad.letsgolive

import android.content.Context
import android.support.multidex.MultiDexApplication
import com.saad.letsgolive.network.LGLApi
import java.lang.ref.WeakReference

/**
 * Created by saurabh on 31-05-2018.
 */

class LGLApp : MultiDexApplication() {

    override fun onCreate() {
        super.onCreate()
        wApp.clear()
        wApp = WeakReference(this)
        //        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
        //                .setDefaultFontPath(getResources().getString(R.string.font_regular))
        //                .setFontAttrId(R.attr.fontPath)
        //                .build()
        //        );
        LGLApi.setAPIEnvironment();
    }

    companion object {
        private var wApp = WeakReference<LGLApp>(null)
        val instance: LGLApp
            get() = wApp.get()!!

        val context: Context
            get() {
                val app = wApp.get()
                return if (app != null) app.applicationContext else LGLApp().applicationContext
            }
    }
}