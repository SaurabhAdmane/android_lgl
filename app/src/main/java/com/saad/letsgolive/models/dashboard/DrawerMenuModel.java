package com.saad.letsgolive.models.dashboard;

/**
 * Created by Saurabh on 6/24/2018.
 */

public class DrawerMenuModel {

    private String strDrawerName;
    private int i_drawerIcon;

    public String getStrDrawerName() {
        return strDrawerName;
    }

    public void setStrDrawerName(String strDrawerName) {
        this.strDrawerName = strDrawerName;
    }

    public int getI_drawerIcon() {
        return i_drawerIcon;
    }

    public void setI_drawerIcon(int i_drawerIcon) {
        this.i_drawerIcon = i_drawerIcon;
    }
}
