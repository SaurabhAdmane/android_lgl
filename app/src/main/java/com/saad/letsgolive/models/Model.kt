package com.saad.letsgolive.models

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty

/**
 * Created by FAYEGU-CONT on 6/29/2018.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
object Model {

    data class ProfileMenuModel(
            var strMenuName: String? = null,
            var i_menuIcon: Int = 0)

    data class ProfileMenuModelTopView(
            var strMenuName: String? = null,
            var i_menuIcon: Int = 0)

    data class ProfileRequestBean(
            @JsonProperty("email")
            var email: String? = null,
            @JsonProperty("gender")
            var gender: String? = null,
            @JsonProperty("city")
            var city: String? = null,
            @JsonProperty("state")
            var state: String? = null,
            @JsonProperty("country")
            var country: String? = null,
            @JsonProperty("experience")
            var experience: String? = null,
            @JsonProperty("bio")
            var bio: String? = null,
            @JsonProperty("work")
            var work: String? = null,
            @JsonProperty("school")
            var school: String? = null,
            @JsonProperty("interest")
            var interest: String? = null,
            @JsonProperty("coins")
            var coins: String? = null,
            @JsonProperty("fans")
            var fans: String? = null,
            @JsonProperty("followings")
            var followings: String? = null,
            @JsonProperty("badge")
            var badge: String? = null,
            @JsonProperty("icon_level")
            var level: String? = null,
            @JsonProperty("diamonds")
            var diamonds: String? = null,
            @JsonProperty("qrcode")
            var qrcode: String? = null,
            @JsonProperty("talent")
            var talent: String? = null,
            @JsonProperty("likes")
            var likes: String? = null,
            @JsonProperty("views")
            var views: String? = null,
            @JsonProperty("first_name")
            var first_name: String? = null,
            @JsonProperty("last_name")
            var last_name: String? = null,
            @JsonProperty("mobile")
            var mobile: String? = null,
            @JsonProperty("dob")
            var dob: String? = null,
            @JsonProperty("udid")
            var udid: String? = null)

    //data class ProfileResponseBean()

    data class SignupRequestBean(
            @JsonProperty("first_name")
            var firstName: String? = null,
            @JsonProperty("last_name")
            var lastName: String? = null,
            @JsonProperty("mobile")
            var mobile: String? = null,
            @JsonProperty("dob")
            var dob: String? = null,
            @JsonProperty("email")
            var email: String? = null,
            @JsonProperty("password")
            var password: String? = null,
            @JsonProperty("gender")
            var gender: String? = null,
            @JsonProperty("city")
            var city: String? = null,
            @JsonProperty("state")
            var state: String? = null,
            @JsonProperty("country")
            var country: String? = null,
            @JsonProperty("exprience")
            var exprience: String? = null,
            @JsonProperty("coins")
            var coins: String? = null,
            @JsonProperty("fans")
            var fans: String? = null,
            @JsonProperty("following")
            var following: String? = null,
            @JsonProperty("badge")
            var badge: String? = null,
            @JsonProperty("icon_level")
            var level: String? = null,
            @JsonProperty("diamonds")
            var diamonds: String? = null,
            @JsonProperty("qrcode")
            var qrcode: String? = null,
            @JsonProperty("bio")
            var bio: String? = null,
            @JsonProperty("school")
            var school: String? = null,
            @JsonProperty("work")
            var work: String? = null,
            @JsonProperty("interest")
            var interest: String? = null,
            @JsonProperty("device_id")
            var deviceId: String? = null,
            @JsonProperty("udid")
            var udid: String? = null,
            @JsonProperty("talent")
            var talent: String? = null,
            @JsonProperty("likes")
            var likes: String? = null,
            @JsonProperty("views")
            var views: String? = null)

}