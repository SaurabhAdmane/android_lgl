package com.saad.letsgolive.models.profile;

/**
 * Created by saurabha on 6/13/2018.
 */

public class ProfileMenuModel {

    private String strMenuName;
    private int i_menuIcon;

    public String getStrMenuName() {
        return strMenuName;
    }

    public void setStrMenuName(String strMenuName) {
        this.strMenuName = strMenuName;
    }

    public int getI_menuIcon() {
        return i_menuIcon;
    }

    public void setI_menuIcon(int i_menuIcon) {
        this.i_menuIcon = i_menuIcon;
    }
}
