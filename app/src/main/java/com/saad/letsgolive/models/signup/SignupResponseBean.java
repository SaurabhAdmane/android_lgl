package com.saad.letsgolive.models.signup;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Saurabh on 6/7/2018.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SignupResponseBean {

    @JsonProperty("status_code")
    private int statusCode;
    @JsonProperty("flag")
    private boolean flag;
    @JsonProperty("message")
    private String message;
    @JsonProperty("user")
    private User user;
    @JsonProperty("user_details")
    private Object userDetails;
    @JsonProperty("user_profile")
    private Object userProfile;

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Object getUserDetails() {
        return userDetails;
    }

    public void setUserDetails(Object userDetails) {
        this.userDetails = userDetails;
    }

    public Object getUserProfile() {
        return userProfile;
    }

    public void setUserProfile(Object userProfile) {
        this.userProfile = userProfile;
    }

    @Override
    public String toString() {
        return "SignupResponseBean{" +
                "statusCode=" + statusCode +
                ", flag=" + flag +
                ", message='" + message + '\'' +
                ", user=" + user +
                ", userDetails=" + userDetails +
                ", userProfile=" + userProfile +
                '}';
    }
}

