package com.saad.letsgolive.profile.viewmodel

import android.databinding.ObservableField
import com.saad.letsgolive.LGLApp
import com.saad.letsgolive.R
import com.saad.letsgolive.baseclass.BaseViewModel
import com.saad.letsgolive.models.Model
import com.saad.letsgolive.models.profile.ProfileRequestBean
import com.saad.letsgolive.profile.ProfileManager
import io.reactivex.disposables.CompositeDisposable

/**
 * Created by Saurabh on 6/11/2018.
 */
class ProfileViewModel : BaseViewModel() {

    var fanCount: ObservableField<String>
    var followingCount: ObservableField<String>
    var profileManager: ProfileManager
    val compositeDisposable: CompositeDisposable

    init {
        compositeDisposable = CompositeDisposable()
        profileManager = ProfileManager(compositeDisposable)
        fanCount = ObservableField("")
        followingCount = ObservableField("")
    }

    fun getRewardMenu(): ArrayList<Int> {
        val rewardsMenu: ArrayList<Int> = ArrayList()
        rewardsMenu.add(0, R.drawable.icon_diamond)
        rewardsMenu.add(1, R.drawable.icon_lgl_coin)
        rewardsMenu.add(2, R.drawable.icon_level)
        return rewardsMenu
    }

    fun getProfileMenu(): ArrayList<Model.ProfileMenuModel> {
        val menuList: ArrayList<Model.ProfileMenuModel> = ArrayList()
        val menu_1 = Model.ProfileMenuModel()
        // Recharge, Income, Transaction History, Event Center, VIP, My Badge
        menu_1.strMenuName = LGLApp.context.getString(R.string.str_recharge)
        menu_1.i_menuIcon = R.drawable.recharge
        menuList.add(0, menu_1)

        val menu_2 = Model.ProfileMenuModel()
        menu_2.strMenuName = LGLApp.context.getString(R.string.str_income)
        menu_2.i_menuIcon = R.drawable.income
        menuList.add(1, menu_2)

        val menu_3 = Model.ProfileMenuModel()
        menu_3.strMenuName = LGLApp.context.getString(R.string.str_txnhis)
        menu_3.i_menuIcon = R.drawable.transaction_history
        menuList.add(2, menu_3)

        val menu_4 = Model.ProfileMenuModel()
        menu_4.strMenuName = LGLApp.context.getString(R.string.str_eventcenter)
        menu_4.i_menuIcon = R.drawable.event_center
        menuList.add(3, menu_4)

        val menu_5 = Model.ProfileMenuModel()
        menu_5.strMenuName = LGLApp.context.getString(R.string.str_vip)
        menu_5.i_menuIcon = R.drawable.vip
        menuList.add(4, menu_5)

        val menu_6 = Model.ProfileMenuModel()
        menu_6.strMenuName = LGLApp.context.getString(R.string.str_my_badge)
        menu_6.i_menuIcon = R.drawable.my_badge
        menuList.add(5, menu_6)

        val menu_7 = Model.ProfileMenuModel()
        menu_7.strMenuName = LGLApp.context.getString(R.string.str_my_bag)
        menu_7.i_menuIcon = R.drawable.my_bag
        menuList.add(6, menu_7)

        val menu_8 = Model.ProfileMenuModel()
        menu_8.strMenuName = LGLApp.context.getString(R.string.str_store)
        menu_8.i_menuIcon = R.drawable.store
        menuList.add(7, menu_8)

        return menuList
    }

    public fun updateProfile(requestBean: ProfileRequestBean) {
        profileManager.updateProfile(requestBean, {

        }, { throwable ->

        })
    }

    override fun subscribe() {}

    override fun clear() {
        profileManager.clear()
    }

    override fun dispose() {
        profileManager.dispose()
    }

}