package com.saad.letsgolive.profile.dataprovider;

import com.saad.letsgolive.LGLApp;
import com.saad.letsgolive.models.profile.ProfileRequestBean;
import com.saad.letsgolive.models.profile.ProfileResponseBean;
import com.saad.letsgolive.profile.ProfileServices;
import com.saad.letsgolive.utils.CommonUtils;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

import static com.saad.letsgolive.network.RestClient.getClient;

/**
 * Created by Saurabh on 6/20/2018.
 */

public class ProfileNetworkDataProvider implements ProfileDataProvider {

    private static ProfileNetworkDataProvider profileNetworkDataProvider;
    private ProfileServices profileServices;

    private ProfileNetworkDataProvider() {
        profileServices = getClient().create(ProfileServices.class);
    }

    public static synchronized ProfileNetworkDataProvider getInstance() {
        if (profileNetworkDataProvider == null)
            profileNetworkDataProvider = new ProfileNetworkDataProvider();
        return profileNetworkDataProvider;
    }

    private Observable<ProfileResponseBean> updateProfile(ProfileRequestBean requestBean) {
        return profileServices.updateProfile(requestBean)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Disposable updateProfileDisposable(ProfileRequestBean generateOTPRequestBean, final Consumer<ProfileResponseBean> success, Consumer<Throwable> error) {
        if ( CommonUtils.INSTANCE.canConnect(LGLApp.Companion.getInstance())) {
            return updateProfile(generateOTPRequestBean).subscribe(new Consumer<ProfileResponseBean>() {
                @Override
                public void accept(ProfileResponseBean profileResponseBean) throws Exception {
                    success.accept(profileResponseBean);
                }
            }, error);
        }
        return Observable.just(0).subscribe();
    }
}
