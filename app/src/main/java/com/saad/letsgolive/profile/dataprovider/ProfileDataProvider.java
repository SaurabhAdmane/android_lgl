package com.saad.letsgolive.profile.dataprovider;

import com.saad.letsgolive.models.profile.ProfileRequestBean;
import com.saad.letsgolive.models.profile.ProfileResponseBean;

import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

/**
 * Created by Saurabh on 6/15/2018.
 */

public interface ProfileDataProvider {

    Disposable updateProfileDisposable(ProfileRequestBean generateOTPRequestBean, Consumer<ProfileResponseBean> success, Consumer<Throwable> error);
}
