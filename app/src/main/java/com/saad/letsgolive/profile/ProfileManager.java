package com.saad.letsgolive.profile;

import com.saad.letsgolive.models.profile.ProfileRequestBean;
import com.saad.letsgolive.models.profile.ProfileResponseBean;
import com.saad.letsgolive.profile.dataprovider.ProfileNetworkDataProvider;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

/**
 * Created by Saurabh on 6/15/2018.
 */

public class ProfileManager {

    private ProfileNetworkDataProvider profileDataProvider;
    private CompositeDisposable compositeDisposable;
    private Disposable profileDisposable;

    public ProfileManager(CompositeDisposable compositeDisposable) {
        this.compositeDisposable = compositeDisposable;
        profileDataProvider = ProfileNetworkDataProvider.getInstance();
    }

    public void updateProfile(ProfileRequestBean requestBean, final Consumer<ProfileResponseBean> success, Consumer<Throwable> error) {
        profileDisposable = profileDataProvider.updateProfileDisposable(requestBean, new Consumer<ProfileResponseBean>() {
            @Override
            public void accept(ProfileResponseBean boughtGiftVoucherResponseBeen) throws Exception {
                success.accept(boughtGiftVoucherResponseBeen);
            }
        }, error);
        if (profileDisposable != null)
            compositeDisposable.add(profileDisposable);
    }

    public void clear() {
        if (compositeDisposable != null)
            compositeDisposable.clear();
    }

    public void dispose() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed())
            compositeDisposable.dispose();
    }
}
