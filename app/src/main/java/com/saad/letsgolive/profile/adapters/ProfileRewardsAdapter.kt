package com.saad.letsgolive.profile.adapters

import android.content.Context
import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.saad.letsgolive.R
import com.saad.letsgolive.databinding.ItemRewardsTextBinding

/**
 * Created by saurabha on 3/12/2018.
 */

class ProfileRewardsAdapter(private val mContext: Context, private val arrayList: ArrayList<Int>) :
        RecyclerView.Adapter<ProfileRewardsAdapter.ItemViewHolder>() {

    private val mInflater: LayoutInflater = mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val binding = DataBindingUtil.inflate<ItemRewardsTextBinding>(mInflater, R.layout.item_rewards_text, parent, false)
        return ItemViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.setData(arrayList[position], position)
    }

    override fun getItemCount(): Int {
        return arrayList.size
    }

    inner class ItemViewHolder(private val binding: ItemRewardsTextBinding) : RecyclerView.ViewHolder(binding.root) {
        fun setData(bean: Int, position: Int) {
            binding.imgItemName.setImageResource(bean)
        }
    }
}