package com.saad.letsgolive.profile

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.SimpleItemAnimator
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.saad.letsgolive.R
import com.saad.letsgolive.baseclass.BaseFragment
import com.saad.letsgolive.databinding.FragmentProfileBinding
import com.saad.letsgolive.profile.adapters.ProfileMenuAdapter
import com.saad.letsgolive.profile.adapters.ProfileRewardsAdapter
import com.saad.letsgolive.profile.viewmodel.ProfileViewModel

/**
 * Created by Saurabh on 6/11/2018.
 */
class ProfileFragment : BaseFragment() {

    private var binding: FragmentProfileBinding? = null
    private var viewModel: ProfileViewModel? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_profile, container, false)
        viewModel = ProfileViewModel()
        binding?.viewModel = viewModel

        initRewardsRecycler()
        initMenuRecycler()
        return binding?.root
    }

    private fun initMenuRecycler() {
        val recyclerView = binding?.recyclerMenu
        val animator = recyclerView?.itemAnimator
        if (animator is SimpleItemAnimator) {
            animator.supportsChangeAnimations = false
        }
        val menuRecyclerAdapter = activity?.let { ProfileMenuAdapter(it, viewModel?.getProfileMenu()!!) }
        recyclerView?.setHasFixedSize(true)
        recyclerView?.layoutManager = GridLayoutManager(activity, 3)
        val itemDecoration = activity?.let { ItemOffsetDecoration(it, R.dimen._5sdp) }
        recyclerView?.addItemDecoration(itemDecoration)
        recyclerView?.adapter = menuRecyclerAdapter

    }

    private fun initRewardsRecycler() {
        val recyclerView = binding?.recyclerRewards
        val animator = recyclerView?.itemAnimator
        if (animator is SimpleItemAnimator) {
            animator.supportsChangeAnimations = false
        }
        val rewardsAdapter = activity?.let { ProfileRewardsAdapter(it, viewModel?.getRewardMenu()!!) }
        recyclerView?.layoutManager = GridLayoutManager(activity, 3)
        val itemDecoration = activity?.let { ItemOffsetDecoration(it, R.dimen._8sdp) }
        recyclerView?.addItemDecoration(itemDecoration)
        recyclerView?.adapter = rewardsAdapter
    }
}