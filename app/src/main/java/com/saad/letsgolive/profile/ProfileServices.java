package com.saad.letsgolive.profile;

import com.saad.letsgolive.models.profile.ProfileRequestBean;
import com.saad.letsgolive.models.profile.ProfileResponseBean;
import com.saad.letsgolive.network.LGLApi;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.POST;


/**
 * Created by Saurabh on 6/15/2018.
 */

public interface ProfileServices {

        @POST(LGLApi.ENDPOINT_PROFILE)
//    @Encrypted
    Observable<ProfileResponseBean> updateProfile(@Body ProfileRequestBean perProfileRequestBean);


}
