package com.saad.letsgolive.signupLogin.dataproviders;

import com.saad.letsgolive.models.signup.LoginRequestBean;
import com.saad.letsgolive.models.signup.SignupRequestBean;
import com.saad.letsgolive.models.signup.SignupResponseBean;

import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

/**
 * Created by saurabha on 19-09-2017.
 */

public interface SignupDataProvider {

    Disposable signupUserDisposable(SignupRequestBean signupRequestBean, Consumer<SignupResponseBean> success, Consumer<Throwable> error);

    Disposable loginUserDisposable(LoginRequestBean loginRequestBean, Consumer<SignupResponseBean> consumer, Consumer<Throwable> error);
}
