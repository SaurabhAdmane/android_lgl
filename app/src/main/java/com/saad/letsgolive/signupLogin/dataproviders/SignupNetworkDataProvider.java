package com.saad.letsgolive.signupLogin.dataproviders;

import com.saad.letsgolive.LGLApp;
import com.saad.letsgolive.models.signup.LoginRequestBean;
import com.saad.letsgolive.models.signup.SignupRequestBean;
import com.saad.letsgolive.models.signup.SignupResponseBean;
import com.saad.letsgolive.signupLogin.SignupServices;
import com.saad.letsgolive.utils.CommonUtils;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

import static com.saad.letsgolive.network.RestClient.getClient;

/**
 * Created by saurabha on 21-09-2017.
 */

public class SignupNetworkDataProvider implements SignupDataProvider {

    private static SignupNetworkDataProvider signupNetworkInstance;
    private SignupServices signupServices;

    private SignupNetworkDataProvider() {
        signupServices = getClient().create(SignupServices.class);
    }

    public static synchronized SignupNetworkDataProvider getInstance() {
        if (signupNetworkInstance == null)
            signupNetworkInstance = new SignupNetworkDataProvider();
        return signupNetworkInstance;
    }

    private Observable<SignupResponseBean> signupUser(SignupRequestBean requestBean) {
        return signupServices.signupUser(requestBean)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    private Observable<SignupResponseBean> loginUser(LoginRequestBean requestBean) {
        return signupServices.loginUser(requestBean)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Disposable signupUserDisposable(SignupRequestBean signupRequestBean, final Consumer<SignupResponseBean> responseBean, final Consumer<Throwable> error) {
//        TMWObjectRequestBean<SignupRequestBean> reqBean = new TMWObjectRequestBean<>();
//        reqBean.setCvib(getCVIB(false));
//        reqBean.setData(signupRequestBean);
        if (CommonUtils.INSTANCE.canConnect(LGLApp.Companion.getInstance())) {
            return signupUser(signupRequestBean).subscribe(new Consumer<SignupResponseBean>() {
                @Override
                public void accept(SignupResponseBean response) throws Exception {
                    if (response != null) {
                        responseBean.accept(response);
                        return;
                    } else {
                        error.accept(new Throwable(response.getMessage()));
                    }
                }
            }, error);
        }
        return Observable.just(0).subscribe();
    }

    @Override
    public Disposable loginUserDisposable(LoginRequestBean loginRequestBean, Consumer<SignupResponseBean> responseBean, Consumer<Throwable> error) {
        if (CommonUtils.INSTANCE.canConnect(LGLApp.Companion.getInstance())) {
            return loginUser(loginRequestBean).subscribe(new Consumer<SignupResponseBean>() {
                @Override
                public void accept(SignupResponseBean response) throws Exception {
                    if (response != null) {
                        responseBean.accept(response);
                        return;
                    }
                    error.accept(new Throwable(response.getMessage()));
                }
            }, error);
        }
        return Observable.just(0).subscribe();
    }

}