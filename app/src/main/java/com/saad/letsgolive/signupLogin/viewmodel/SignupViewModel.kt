package com.saad.letsgolive.signupLogin.viewmodel

import android.databinding.ObservableField
import android.text.TextUtils
import com.saad.letsgolive.LGLApp
import com.saad.letsgolive.R
import com.saad.letsgolive.baseclass.BaseViewModel
import com.saad.letsgolive.models.signup.SignupRequestBean
import com.saad.letsgolive.models.signup.SignupResponseBean
import com.saad.letsgolive.signupLogin.SignupManager
import com.saad.letsgolive.utils.CommonUtils
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Consumer

/**
 * Created by Saurabh on 7/3/2018.
 */
class SignupViewModel : BaseViewModel() {

    private var compositeDisposable: CompositeDisposable
    private var signupManager: SignupManager
    var UserFirstName: ObservableField<String>
    var UserLastName: ObservableField<String>
    var EmailAddress: ObservableField<String>
    var UserDOB: ObservableField<String>
    var UserGender: ObservableField<String>
    var Password: ObservableField<String>
    var Mobile: ObservableField<String>
    var Country: ObservableField<String>
    var State: ObservableField<String>
    var City: ObservableField<String>

    init {
        compositeDisposable = CompositeDisposable()
        signupManager = SignupManager(compositeDisposable)
        UserFirstName = ObservableField<String>("")
        UserLastName = ObservableField<String>("")
        EmailAddress = ObservableField<String>("")
        UserDOB = ObservableField<String>("")
        UserGender = ObservableField<String>("")
        Password = ObservableField<String>("")
        Mobile = ObservableField<String>("")
        Country = ObservableField<String>("")
        State = ObservableField<String>("")
        City = ObservableField<String>("")
    }

    fun validateData(response: Consumer<SignupResponseBean>, error: Consumer<Throwable>) {
        val strUserFirstName = UserFirstName.get()
        val strUserLastName = UserLastName.get()
        val strEmailAddress = EmailAddress.get()
        val strUserDOB = UserDOB.get()
        val strUserGender = UserGender.get()
        val strPassword = Password.get()
        val strMobile = Mobile.get()
        val strCountry = Country.get()
        val strState = State.get()
        val strCity = City.get()

        if (!TextUtils.isEmpty(strUserFirstName)) {
            if (!TextUtils.isEmpty(strUserLastName)) {
                if (!TextUtils.isEmpty(strEmailAddress)) {
                    if (!TextUtils.isEmpty(strUserDOB)) {
                        if (!TextUtils.isEmpty(strMobile)) {
                            if (!TextUtils.isEmpty(strPassword)) {
                                if (strPassword!!.length < 8) {
                                    //if remaining params are blank then set those params " " and send it to api as currently all params are required
                                    //Call Sign Up Api here
                                    val requestBean = SignupRequestBean()
                                    requestBean.firstName = strUserFirstName
                                    requestBean.lastName = strUserLastName
                                    requestBean.mobile = strMobile
                                    requestBean.dob = strUserDOB
                                    requestBean.email = strEmailAddress
                                    requestBean.password = strPassword
                                    requestBean.gender = strUserGender
                                    requestBean.city = strCity
                                    requestBean.state = strState
                                    requestBean.country = strCountry
                                    requestBean.exprience = ""
                                    requestBean.coins = ""
                                    requestBean.fans = ""
                                    requestBean.following = ""
                                    requestBean.badge = ""
                                    requestBean.level = ""
                                    requestBean.diamonds = ""
                                    requestBean.qrcode = ""
                                    requestBean.bio = ""
                                    requestBean.school = ""
                                    requestBean.work = ""
                                    requestBean.interest = ""
                                    requestBean.deviceId = ""
                                    requestBean.udid = ""
                                    requestBean.talent = ""
                                    requestBean.likes = ""
                                    requestBean.views = ""
                                    signupUser(requestBean, response, error);
                                } else {
                                    CommonUtils.showToastMsg(LGLApp.context.getString(R.string.str_min_pas_length))
                                }
                            } else {
                                CommonUtils.showToastMsg(LGLApp.context.getString(R.string.str_password_blank))
                            }
                        } else {
                            CommonUtils.showToastMsg(LGLApp.context.getString(R.string.str_mobile_no_blank))
                        }
                    } else {
                        CommonUtils.showToastMsg(LGLApp.context.getString(R.string.str_dob_blank))
                    }
                } else {
                    CommonUtils.showToastMsg(LGLApp.context.getString(R.string.str_email_blank))
                }
            } else {
                CommonUtils.showToastMsg(LGLApp.context.getString(R.string.str_lastname_blank))
            }
        } else {
            CommonUtils.showToastMsg(LGLApp.context.getString(R.string.str_username_blank))
        }
    }

    fun signupUser(signupRequestBean: SignupRequestBean, response: Consumer<SignupResponseBean>, error: Consumer<Throwable>) {
        dialogVisibility.onNext(true)
        signupManager.signupUser(signupRequestBean, { signupResponseBean ->
            dialogVisibility.onNext(false)
            CommonUtils.saveUserDatta(signupResponseBean)
            response.accept(signupResponseBean)
        }, { throwable ->
            dialogVisibility.onNext(false)
            error.accept(throwable)
        })
    }

    override fun subscribe() {
    }

    override fun clear() {
    }

    override fun dispose() {}

}