package com.saad.letsgolive.signupLogin.viewmodel

import android.databinding.ObservableField
import android.text.TextUtils
import com.saad.letsgolive.LGLApp
import com.saad.letsgolive.R
import com.saad.letsgolive.baseclass.BaseViewModel
import com.saad.letsgolive.models.signup.LoginRequestBean
import com.saad.letsgolive.signupLogin.SignupManager
import com.saad.letsgolive.utils.CommonUtils
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Consumer

/**
 * Created by Saurabh on 6/3/2018.
 */
class LoginViewModel : BaseViewModel() {

    private var compositeDisposable: CompositeDisposable
    private var signupManager: SignupManager
    var email: ObservableField<String>
    var password: ObservableField<String>

    init {
        compositeDisposable = CompositeDisposable()
        signupManager = SignupManager(compositeDisposable)
        email = ObservableField<String>("")
        password = ObservableField<String>("")
    }

    fun loginUser(signupRequestBean: LoginRequestBean, response: Consumer<Boolean>, error: Consumer<Throwable>) {
        dialogVisibility.onNext(true)
        signupManager.loginUser(signupRequestBean, { signupResponseBean ->
            dialogVisibility.onNext(false)
            if (signupResponseBean.isFlag) {
                CommonUtils.saveUserDatta(signupResponseBean)
                response.accept(signupResponseBean.isFlag)
            } else {
                error.accept(Throwable(signupResponseBean.message))
            }
        }, { throwable ->
            dialogVisibility.onNext(false)
            error.accept(throwable)
        })
    }

    override fun subscribe() {

    }

    override fun clear() {

    }

    override fun dispose() {

    }

    fun isValidData(response: Consumer<Boolean>, error: Consumer<Throwable>) {
        val strEmail = email.get()
        val strPassword = password.get()
        if (CommonUtils.canConnect(LGLApp.context)) {
            if (TextUtils.isEmpty(strEmail) && TextUtils.isEmpty(strPassword)) {
                CommonUtils.showToastMsg(LGLApp.context.getString(R.string.str_emailpass_blank))
            } else {
                if (!TextUtils.isEmpty(strEmail)) {
                    if (!TextUtils.isEmpty(strPassword)) {
                        val request = LoginRequestBean()
                        request.email = strEmail
                        request.password = strPassword
                        loginUser(request, response, error)
                    } else {
                        CommonUtils.showToastMsg(LGLApp.context.getString(R.string.str_passcantblank))
                    }
                } else {
                    CommonUtils.showToastMsg(LGLApp.context.getString(R.string.str_emailcantblank))
                }
            }
        } else {
            CommonUtils.showToastMsg(LGLApp.context.getString(R.string.connecttointernet))
        }
    }
}