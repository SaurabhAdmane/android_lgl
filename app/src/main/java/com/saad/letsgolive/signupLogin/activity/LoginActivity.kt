package com.saad.letsgolive.signupLogin.activity

import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.view.WindowManager
import com.facebook.AccessToken
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.firebase.auth.FacebookAuthProvider
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider
import com.google.firebase.auth.TwitterAuthProvider
import com.saad.letsgolive.R
import com.saad.letsgolive.dashboard.views.DashboardActivity
import com.saad.letsgolive.databinding.ActivityLoginBinding
import com.saad.letsgolive.signupLogin.viewmodel.LoginViewModel
import com.saad.letsgolive.utils.CommonUtils
import com.saad.letsgolive.utils.Constants
import com.saad.letsgolive.utils.LGLSharedPreference
import com.twitter.sdk.android.core.Callback
import com.twitter.sdk.android.core.Result
import com.twitter.sdk.android.core.TwitterException
import com.twitter.sdk.android.core.TwitterSession
import com.twitter.sdk.android.core.identity.TwitterLoginButton
import io.reactivex.functions.Consumer
import java.util.*


/**
 * Created by Saurabh on 6/3/2018.
 */
class LoginActivity : AppCompatActivity(), View.OnClickListener, GoogleApiClient.OnConnectionFailedListener {

    private val TAG_TWITTER = "TwitterLogin"
    private val TAG_FB = "FacebookSignIn"
    private var mAuth: FirebaseAuth? = null
    /*
    For Google Login
     */
    private val REQUEST_CODE_GOOGLE_SIGN_IN = 1
    private val TAG_GOOGLE = "GoogleSignIn"
    private var mGoogleApiClient: GoogleApiClient? = null
    /*
    For FB Login
     */
    private var callbackManager: CallbackManager? = null
    /*
    For Twitter Login
     */
    var twitter_login_bt: TwitterLoginButton? = null
    private var viewModel: LoginViewModel? = null
    private var binding: ActivityLoginBinding? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        CommonUtils.configureTwitterSDK()
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView<ActivityLoginBinding>(this, R.layout.activity_login) as ActivityLoginBinding
        setContentView(binding?.root)
        viewModel = LoginViewModel()
        binding?.viewModel = viewModel
        initViews()
        setListeners()
    }

    override fun onResume() {
        super.onResume()
//        if (LGLSharedPreference.getString(Constants.SHARED_FIRST_NAME) != null) {
//            intentToDashBoard()
//        }
    }

    private fun setListeners() {
        binding?.txtUpwardArrowSignUpView?.setOnClickListener(this)
        binding?.txtUpwardArrowSignUp?.setOnClickListener(this)
        binding?.txtSignUpMove?.setOnClickListener(this)
        binding?.imageFacebook?.setOnClickListener(this)
        binding?.imageGoogle?.setOnClickListener(this)
        binding?.imageTwitter?.setOnClickListener(this)
        binding?.btnLogin?.setOnClickListener(this)
    }

    /*
        Function to initialize views
         */
    private fun initViews() {
        mAuth = FirebaseAuth.getInstance()
        window.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
        twitter_login_bt = binding?.twitterLoginButton
        initializeGoogleLogin()
    }

    /*
   Function to initialize google client
    */
    private fun initializeGoogleLogin() {

        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.request_client_id))
                .requestEmail()
                .build()

        mGoogleApiClient = GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent();
        if (requestCode == REQUEST_CODE_GOOGLE_SIGN_IN) {
            val result = Auth.GoogleSignInApi.getSignInResultFromIntent(data)
            if (result.isSuccess) {
                // successful -> authenticate with Firebase
                val account = result.signInAccount
                firebaseAuthWithGoogle(account!!)
            } else {
                CommonUtils.showToastMsg("signInGoogle: failed!")
            }
        } else {
            callbackManager?.onActivityResult(requestCode, resultCode, data)
        }
        twitter_login_bt!!.onActivityResult(requestCode, resultCode, data)
    }

    /*
   Function to start animation of signup view
    */
    private fun startSignUpAnimation() {
        val i2 = Intent(this, SignupActivity::class.java)
        startActivity(i2)
        overridePendingTransition(R.anim.anim_slide_up, R.anim.anim_slide_down)
    }

    /*
    Callback for click event's
     */
    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.imageGoogle -> loginViaGoogle()
            R.id.imageFacebook -> loginViaFaceBook()
            R.id.imageInstagram -> loginViaInstagram()
            R.id.imageTwitter -> loginViaTwitter()
            R.id.txtUpwardArrowSignUpView -> startSignUpAnimation()
            R.id.txtUpwardArrowSignUp -> startSignUpAnimation()
            R.id.txtSignUpMove -> startSignUpAnimation()
            R.id.btnLogin -> viewModel?.isValidData(Consumer { response ->
                if (response)
                    startActivity(Intent(this, DashboardActivity::class.java))
            }, Consumer { throwable ->
                CommonUtils.showToastMsg(throwable.toString())
            })
        }
    }

    /*
    Method to login via google
     */
    private fun loginViaGoogle() {

        val intent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient)
        startActivityForResult(intent, REQUEST_CODE_GOOGLE_SIGN_IN)
    }

    /*
    Function to get Google sign in authenticated user details
     */
    private fun firebaseAuthWithGoogle(acct: GoogleSignInAccount) {
        Log.e(TAG_GOOGLE, "firebaseAuthWithGoogle():" + acct.id!!)

        val credential = GoogleAuthProvider.getCredential(acct.idToken, null)
        mAuth!!.signInWithCredential(credential)
                .addOnCompleteListener(this) { task ->
                    if (task.isSuccessful) {
                        // Sign in success
                        Log.e(TAG_GOOGLE, "signInWithCredential: Success!")
                        val user = mAuth!!.currentUser
                        if (user != null) {
                            Log.e(TAG_GOOGLE, "User:!" + user.email)
                            Log.e(TAG_GOOGLE, "Display Name:!" + user.displayName)
                            Log.e(TAG_GOOGLE, "URL:!" + user.photoUrl)
                            Log.e(TAG_GOOGLE, "Ph No:!" + user.phoneNumber)
                            insertDataToPreferenceHelper(user.displayName!!, user.email!!, user.photoUrl.toString())
                            intentToDashBoard()
                        }
                    } else {
                        // Sign in fails
                        Log.w(TAG_GOOGLE, "signInWithCredential: Failed!", task.exception)
                        CommonUtils.showToastMsg("Authentication failed!")
                    }
                }
    }

    /*
    Function to login via FB
     */
    private fun loginViaFaceBook() {

        callbackManager = CallbackManager.Factory.create()
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "email"))
        LoginManager.getInstance().registerCallback(callbackManager,
                object : FacebookCallback<LoginResult> {
                    override fun onSuccess(loginResult: LoginResult) {

                        Log.d(TAG_FB, "facebook:onSuccess:$loginResult")
                        handleFacebookAccessToken(loginResult.accessToken)
                    }

                    override fun onCancel() {
                        Log.d(TAG_FB, "Facebook onCancel")

                    }

                    override fun onError(error: FacebookException) {
                        Log.d(TAG_FB, "Facebook onError$error")

                    }
                })

    }

    private fun handleFacebookAccessToken(token: AccessToken) {
        Log.d(TAG_FB, "handleFacebookAccessToken:$token")
        val credential = FacebookAuthProvider.getCredential(token.token)
        mAuth!!.signInWithCredential(credential)
                .addOnCompleteListener(this) { task ->
                    if (task.isSuccessful) {
                        // Sign in success, update UI with the signed-in user's information
                        Log.d(TAG_FB, "signInWithCredential:success")
                        val user = mAuth!!.currentUser
                        if (user != null) {
                            Log.d(TAG_FB, "User details : " + user.displayName + user.email + "\n" + user.photoUrl + "\n"
                                    + user.uid + "\n" + user.providerId)
                            insertDataToPreferenceHelper(user.displayName!!, "", user.photoUrl.toString())
                            intentToDashBoard()
                        }
                    } else {
                        // If sign in fails, display a message to the user.
                        Log.w(TAG_FB, "signInWithCredential:failure", task.getException())
                        CommonUtils.showToastMsg("Authentication failed!")
                    }
                }
    }

    /*
    Function to login via Twitter
     */
    private fun loginViaTwitter() {
        twitter_login_bt!!.callback = object : Callback<TwitterSession>() {
            override fun success(result: Result<TwitterSession>) {
                Log.d(TAG_TWITTER, "twitterLogin:success$result")
                handleTwitterSession(result.data)
            }

            override fun failure(exception: TwitterException) {
                Log.w(TAG_TWITTER, "twitterLogin:failure", exception)
            }
        }
        twitter_login_bt!!.performClick()
    }

    private fun handleTwitterSession(session: TwitterSession) {
        Log.d(TAG_TWITTER, "handleTwitterSession:$session")
        val credential = TwitterAuthProvider.getCredential(
                session.authToken.token,
                session.authToken.secret)
        mAuth!!.signInWithCredential(credential)
                .addOnCompleteListener(this) { task ->
                    if (task.isSuccessful) {
                        // Sign in success, update UI with the signed-in user's information
                        Log.d(TAG_TWITTER, "signInWithCredential:success")
                        val user = mAuth!!.currentUser
                        if (user != null) {
                            Log.d(TAG_TWITTER, "User details : " + user.displayName + user.email + "\n" + user.photoUrl + "\n"
                                    + user.uid + "\n" + user.providerId);
                            insertDataToPreferenceHelper(user.displayName!!, "", user.photoUrl.toString())
                            intentToDashBoard()
                        }
                    } else {
                        // If sign in fails, display a message to the user.
                        Log.w(TAG_TWITTER, "signInWithCredential:failure", task.exception)
                        CommonUtils.showToastMsg("Authentication failed!")
                    }
                }
    }

    /*
    Function to login via Instagram
     */
    private fun loginViaInstagram() {

    }

    /*
    Function to intent to dashboard
     */
    private fun intentToDashBoard() {
        if (LGLSharedPreference.getString(Constants.SHARED_FIRST_NAME) == null)
            CommonUtils.showToastMsg(getString(R.string.msgLoginSuccess))
        startActivity(Intent(this, DashboardActivity::class.java))
    }

    /*
    Function to insert data in local storage
     */
    private fun insertDataToPreferenceHelper(displayName: String, email: String, picUrl: String) {
        LGLSharedPreference.insertString(Constants.SHARED_FIRST_NAME, displayName)
        LGLSharedPreference.insertString(Constants.SHARED_EMAIL, email)
        LGLSharedPreference.insertString(Constants.USER_PROFILE_PIC_URL, picUrl)
    }

    //Callback on disconnection of google connection
    override fun onConnectionFailed(connectionResult: ConnectionResult) {
        Log.e(TAG_GOOGLE, "onConnectionFailed():$connectionResult")
        CommonUtils.showToastMsg("Google Play Services error.")
    }
}