package com.saad.letsgolive.signupLogin;

import com.saad.letsgolive.models.signup.LoginRequestBean;
import com.saad.letsgolive.models.signup.SignupRequestBean;
import com.saad.letsgolive.models.signup.SignupResponseBean;
import com.saad.letsgolive.signupLogin.dataproviders.SignupNetworkDataProvider;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

/**
 * Created by saurabha on 21-09-2017.
 */

public class SignupManager {

    private SignupNetworkDataProvider signupDataProvider;
    private CompositeDisposable compositeDisposable;
    private Disposable signupDisposable, loginUserDisposable;

    public SignupManager(CompositeDisposable compositeDisposable) {
        this.compositeDisposable = compositeDisposable;
        signupDataProvider = SignupNetworkDataProvider.getInstance();
    }


    public void signupUser(final SignupRequestBean signupRequestBean, final Consumer<SignupResponseBean> consumer, Consumer<Throwable> error) {
        signupDisposable = signupDataProvider.signupUserDisposable
                (signupRequestBean, new Consumer<SignupResponseBean>() {
                    @Override
                    public void accept(SignupResponseBean signupResponseBean) throws Exception {
                        consumer.accept(signupResponseBean);
                    }
                }, error);
        if (signupDisposable != null)
            compositeDisposable.add(signupDisposable);
    }

    public void loginUser(final LoginRequestBean loginRequestBean, final Consumer<SignupResponseBean> consumer, Consumer<Throwable> error) {
        loginUserDisposable = signupDataProvider.loginUserDisposable
                (loginRequestBean, new Consumer<SignupResponseBean>() {
                    @Override
                    public void accept(SignupResponseBean signupResponseBean) throws Exception {
                        consumer.accept(signupResponseBean);
                    }
                }, error);
        if (loginUserDisposable != null)
            compositeDisposable.add(loginUserDisposable);
    }

    public void clear() {
        if (compositeDisposable != null)
            compositeDisposable.clear();
    }

    public void dispose() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed())
            compositeDisposable.dispose();
    }
}
