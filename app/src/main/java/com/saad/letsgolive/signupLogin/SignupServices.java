package com.saad.letsgolive.signupLogin;

import com.saad.letsgolive.models.signup.LoginRequestBean;
import com.saad.letsgolive.models.signup.SignupRequestBean;
import com.saad.letsgolive.models.signup.SignupResponseBean;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.POST;

import static com.saad.letsgolive.network.LGLApi.ENDPOINT_LOGIN;
import static com.saad.letsgolive.network.LGLApi.ENDPOINT_SIGN_UP;

/**
 * Created by saurabha on 21-09-2017.
 */

public interface SignupServices {

    //    @POST(ENDPOINT_GENERATE_OTP)
//    @Encrypted
//    Observable<TMWObjectResponseBean<GenerateOTPResponseBean>> generateOTP(@Body TMWObjectRequestBean<GenerateOTPRequestBean> moneyTransferReqBean);
//
    @POST(ENDPOINT_SIGN_UP)
//    @Encrypted
    Observable<SignupResponseBean> signupUser(@Body SignupRequestBean moneyTransferReqBean);

    @POST(ENDPOINT_LOGIN)
    Observable<SignupResponseBean> loginUser(@Body LoginRequestBean requestBean);
//
//    @POST(ENDPOINT_SIGN_IN)
//    @Encrypted
//    Observable<TMWObjectResponseBean<UserProfileResponseBean>> loginUser(@Body TMWObjectRequestBean<SignupRequestBean> moneyTransferReqBean);
//
//    @POST(ENDPOINT_SET_MPIN)
//    @Encrypted
//    Observable<TMWObjectResponseBean<MpinResponseBean>> setMpin(@Body TMWObjectRequestBean<MpinRequestBean> reqBean);
//
//    @POST(ENDPOINT_CHANGE_PATTERN)
//    @Encrypted
//    Observable<TMWObjectResponseBean<ChangeSecurityPatternResponseBean>> changeSecPattern(@Body TMWObjectRequestBean<ChangeSecurityPatternRequestBean> reqBean);
//
//    @POST(ENDPOINT_FORGOT_MPIN)
//    @Encrypted
//    Observable<TMWObjectResponseBean<ForgotMpinResponseBean>> forgotMpin(@Body TMWObjectRequestBean<ForgotMpinRequestBean> reqBean);
//
//    @POST(ENDPOINT_CHANGE_MPIN)
//    @Encrypted
//    Observable<TMWObjectResponseBean<ChangeMpinResponseBean>> changeMpin(@Body TMWObjectRequestBean<ChangeMpinRequestBean> reqBean);
//
//    @POST(ENDPOINT_VERIFY_MPIN)
//    @Encrypted
//    Observable<TMWObjectResponseBean<VerifyMpinResponseBean>> verifyMpin(@Body TMWObjectRequestBean<VerifyMpinRequestBean> reqBean);
//
//    @POST(ENDPOINT_VERIFY_SECURITY_QUE)
//    @Encrypted
//    Observable<TMWObjectResponseBean<SecureQuestionResponseBean>> secureQuestion(@Body TMWObjectRequestBean<SecureQuestionRequestBean> reqBean);
//
//    @POST(ENDPOINT_APPLY_REFERRAL_CODE)
//    @Encrypted
//    Observable<TMWObjectResponseBean<ReferralCodeResponseBean>> applyReferralCode(@Body TMWObjectRequestBean<ReferralCodeRequestBean> reqBean);
//
//    @POST(TMWAPI.ENDPOINT_DOC_LIST)
//    @Encrypted(isResponseArray = true)
//    Observable<TMWListResponseBean<DocumentResponseBean>> getDocList(@Body TMWObjectRequestBean reqBean);
//
//    @POST(TMWAPI.ENDPOINT_UPDATE_DOC)
//    @Encrypted
//    Observable<TMWObjectResponseBean<UpdateUserDocResponseBean>> updateDoc(@Body TMWObjectRequestBean reqBean);
}
