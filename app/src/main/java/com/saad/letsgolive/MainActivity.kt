package com.saad.letsgolive

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.text.TextUtils
import com.saad.letsgolive.baseclass.BaseActivity
import com.saad.letsgolive.dashboard.views.DashboardActivity
import com.saad.letsgolive.signupLogin.activity.LoginActivity
import com.saad.letsgolive.utils.Constants
import com.saad.letsgolive.utils.LGLSharedPreference

/**
 * Created by saurabh on 31-05-2018.
 */
class MainActivity : BaseActivity() {
    val SPLASH_MIN_DURATION = 2000

    private val mActivityHandler = Handler()
    private val mStartActivity = Runnable {
        checkLogins()
    }

    private fun checkLogins() {
        if (TextUtils.isEmpty(LGLSharedPreference.getString(Constants.SHARED_ID))) {
            intentToLogin()
        } else {
            intentToDashboard()
        }
    }

    private fun intentToLogin() {
        val intentLogin = Intent(this, LoginActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or
                Intent.FLAG_ACTIVITY_CLEAR_TASK or
                Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intentLogin)
    }

    private fun intentToDashboard() {
        val intentDashboard = Intent(this, DashboardActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or
                Intent.FLAG_ACTIVITY_CLEAR_TASK or
                Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intentDashboard)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val t = object : Thread() {
            override fun run() {
                try {
                    Thread.sleep(SPLASH_MIN_DURATION.toLong())
                } catch (exception: Exception) {
                }

                mActivityHandler.post(mStartActivity)
            }
        }
        t.start()
    }
}
