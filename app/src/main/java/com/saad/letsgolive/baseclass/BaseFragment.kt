package com.saad.letsgolive.baseclass

import android.Manifest
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.support.v4.content.LocalBroadcastManager
import android.support.v4.view.MenuItemCompat
import android.support.v4.view.ViewPager
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.app.AppCompatDialog
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.SearchView
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import android.widget.AdapterView
import android.widget.ImageView
import android.widget.Toast
import com.saad.letsgolive.LGLApp
import com.saad.letsgolive.utils.Constants.NO_NETWORK_ACTION
import com.saad.letsgolive.utils.PermissionHandler
import io.reactivex.android.schedulers.AndroidSchedulers

/**
 * Created by saurabh on 01-06-2018.
 */

abstract class BaseFragment : Fragment() {
    protected var baseViewModel: BaseViewModel? = null
    internal var permissionHandler: PermissionHandler? = null
    private val broadcastReceiver: BroadcastReceiver? = null
    private var networkReceiver: BroadcastReceiver? = null
    private val dialog: AppCompatDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setUpNetworkReceiver()
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onPause() {
        super.onPause()
    }

    override fun onStart() {
        if (baseViewModel != null) {
            baseViewModel!!.subscribe()
            baseViewModel!!.getDialogVisibility().subscribe { showDialog ->
                if (null != dialog) {
                    if (showDialog!!)
                        dialog.show()
                    else if (dialog.isShowing)
                        dialog.dismiss()

                }
            }
        }
        super.onStart()
    }

    override fun onStop() {
        if (baseViewModel != null) {
            baseViewModel!!.clear()
        }
        super.onStop()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        unbindView(view)
        releaseMemory()
    }

    override fun onDestroy() {
        super.onDestroy()

        if (baseViewModel != null) {
            baseViewModel!!.dispose()
            baseViewModel!!.getDialogVisibility().unsubscribeOn(AndroidSchedulers.mainThread())
            //   baseViewModel.dialogVisibility = null;
            baseViewModel = null
        }
        if (null != broadcastReceiver) {
            activity!!.unregisterReceiver(broadcastReceiver)
        }

        if (networkReceiver != null) {
            LocalBroadcastManager.getInstance(LGLApp.context).unregisterReceiver(networkReceiver!!)
            networkReceiver = null
        }

        permissionHandler = null
        //   dialog = null;
    }

    fun releaseMemory() {

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            android.R.id.home -> {
                goBack()
                return true
            }

            else -> return super.onOptionsItemSelected(item)
        }
    }

    private fun goBack() {
        if (fragmentManager!!.backStackEntryCount > 0) {
            fragmentManager!!.popBackStack()
        }
    }

    protected fun checkPermissions(permission: Array<String>): Boolean {
        if (null != activity) {
            permissionHandler = PermissionHandler(activity!!)
            return permissionHandler!!.checkPermission(permission, false, this@BaseFragment)
        }
        return false
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            PermissionHandler.CAMERA_PERMISSION -> if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                onPermissionSuccess(PermissionHandler.CAMERA_PERMISSION)
            } else if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_DENIED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(activity!!, Manifest.permission.CAMERA)) {
                    Toast.makeText(activity, "CAMERA permission is required to scan the QR code.", Toast.LENGTH_LONG).show()
                } else {
                    Toast.makeText(context, "Enable CAMERA permission in Permissions option.", Toast.LENGTH_LONG).show()
                }
            }

            PermissionHandler.READ_CONTACTS_PERMISSION -> if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Log.d(TAG, "permission was granted, yay!")
                onPermissionSuccess(PermissionHandler.READ_CONTACTS_PERMISSION)
            } else if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_DENIED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(activity!!, Manifest.permission.READ_CONTACTS)) {
                    Log.d(TAG, "permission denied")
                    Toast.makeText(context, "CONTACT permission is required to read your contacts.", Toast.LENGTH_LONG).show()
                } else {
                    Log.d(TAG, "permission denied permanently")
                    Toast.makeText(context, "Enable CONTACTS permission in Permissions option.", Toast.LENGTH_LONG).show()
                    /*  Intent intent = new Intent();
                        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", getContext().getPackageName(), null);
                        intent.setData(uri);
                        startActivity(intent);*/
                }
            }

            PermissionHandler.READ_SMS_PERMISSION -> if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Log.d(TAG, "permission was granted, yay!")
                onPermissionSuccess(PermissionHandler.READ_SMS_PERMISSION)
                //                    broadcastReceiver = new SmsReceiver();
                activity!!.registerReceiver(broadcastReceiver, IntentFilter("android.provider.Telephony.SMS_RECEIVED"))
            } else if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_DENIED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(activity!!, Manifest.permission.READ_SMS)) {
                    Log.d(TAG, "permission denied")
                    Toast.makeText(context, "SMS permission is required to read relevant SMS.", Toast.LENGTH_LONG).show()
                } else {
                    Log.d(TAG, "permission denied permanently")
                    Toast.makeText(context, "Enable SMS permission in Permissions option.", Toast.LENGTH_LONG).show()
                    /* Intent intent = new Intent();
                        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", getContext().getPackageName(), null);
                        intent.setData(uri);
                        startActivity(intent);*/
                }
            }
        }
    }

    fun onPermissionSuccess(permission: Int) {

    }

    private fun setUpNetworkReceiver() {
        networkReceiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent?) {
                if (intent != null && intent.action != null) {
                    if (NO_NETWORK_ACTION.equals(intent.action!!, ignoreCase = true))
                        if (dialog != null && dialog.isShowing)
                            dialog.dismiss()
                }
            }
        }
        LocalBroadcastManager.getInstance(LGLApp.context).registerReceiver(networkReceiver!!, IntentFilter(NO_NETWORK_ACTION))
    }

    protected fun showNonDismissedDialog() {
        if (dialog != null) {
            dialog.setCanceledOnTouchOutside(false)
            dialog.show()
            dialog.setCancelable(false)
        }
    }

    protected fun dismissProgressDialog() {
        if (dialog != null && dialog.isShowing)
            dialog.dismiss()
    }

    protected fun addFragment(containerViewId: Int,
                              fragment: Fragment,
                              fragmentTag: String) {
        fragmentManager!!
                .beginTransaction()
                .add(containerViewId, fragment, fragmentTag)
                .disallowAddToBackStack()
                .commit()
    }

    protected fun replaceFragment(containerViewId: Int,
                                  fragment: Fragment,
                                  fragmentTag: String,
                                  backStackStateName: String) {
        fragmentManager!!.beginTransaction()
                .replace(containerViewId, fragment, fragmentTag)
                .addToBackStack(backStackStateName)
                .commit()
    }

    companion object {

        private val TAG = BaseFragment::class.java.simpleName

        fun unbindView(v: View?) {
            if (v != null) {
                val count = (v as ViewGroup).childCount
                for (i in 0 until count) {
                    try {
                        val view = v.getChildAt(i)
                        if (view is ViewGroup && view !is AdapterView<*> && view !is ViewPager && view !is RecyclerView)
                            unbindView(view)
                        else {
                            if (v !is SwipeRefreshLayout) {
                                v.removeViewAt(i)
                            }
                        }
                        clearView(view)
                    } catch (e: Exception) {
                        //                    Timber.i("View Exception found" + e.getMessage());
                        println("View Exception found" + e.message)
                    }

                }
            }
        }

        private fun clearView(view: View?) {
            var view = view
            //        LogUtils.SOPLLOGS(TAG ,"View Name: " + restoreEntry.getClass().getSimpleName());
            if (view is RecyclerView) {
                view.adapter = null
                view.clearOnScrollListeners()
                view.layoutManager = null
            } else if (view is ImageView) {
                view.setImageDrawable(null)
            } else if (view is WebView) {
                //            ((WebView)restoreEntry).setOnTouchListener(null);
            } else if (view is Toolbar) {
                val size = view.menu.size()
                for (i in 0 until size) {
                    var item: MenuItem? = view.menu.getItem(i)
                    item!!.setOnMenuItemClickListener(null)
                    MenuItemCompat.setOnActionExpandListener(item, null)
                    if (item.actionView != null && item.actionView is SearchView) {
                        var search: SearchView? = item.actionView as SearchView
                        search!!.setOnQueryTextListener(null)
                        search = null
                    }
                    item = null
                }

                view.setNavigationOnClickListener(null)
                view.setOnMenuItemClickListener(null)
                view.removeAllViews()

            } else if (view is ViewPager) {
                view.adapter = null
                view.removeAllViews()
                view.setOnPageChangeListener(null)
            } else if (view is AdapterView<*>) {
                view.setOnItemClickListener(null)
            } else {
                view!!.setOnClickListener(null)
                view.onFocusChangeListener = null
            }

            view.setOnLongClickListener(null)
            view.setOnTouchListener(null)
            view = null
            //restoreEntry = null;
        }
    }
}
