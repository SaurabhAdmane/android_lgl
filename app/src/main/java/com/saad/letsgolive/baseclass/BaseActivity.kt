package com.saad.letsgolive.baseclass

import android.content.BroadcastReceiver
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.app.AppCompatDialog
import android.view.MenuItem
import com.saad.letsgolive.R
import com.saad.letsgolive.utils.LGLSharedPreference


open class BaseActivity : AppCompatActivity() {

    internal var preference: LGLSharedPreference? = null
    internal var sharedPreferences: SharedPreferences? = null
    private val dialog: AppCompatDialog? = null
    private val logoutReceiver: BroadcastReceiver? = null
    private val helpSupportReceiver: BroadcastReceiver? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        overridePendingTransitionEnter()
    }

    override fun onStart() {
        super.onStart()
    }

    override fun finish() {
        super.finish()
        overridePendingTransitionExit()
    }

    /**
     * Overrides the pending Activity transition by performing the "Enter" animation.
     */
    protected fun overridePendingTransitionEnter() {
        overridePendingTransition(R.anim.anim_slide_in_from_right_enter, R.anim.anim_slide_in_from_right_exit)
    }

    /**
     * Overrides the pending Activity transition by performing the "Exit" animation.
     */
    protected fun overridePendingTransitionExit() {
        overridePendingTransition(R.anim.anim_slide_in_from_left_enter, R.anim.anim_slide_in_from_left_exit)
    }

    override fun startActivity(intent: Intent) {
        super.startActivity(intent)
        overridePendingTransitionEnter()
    }

    override fun supportFinishAfterTransition() {
        super.supportFinishAfterTransition()
        overridePendingTransitionExit()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    protected fun addFragment(containerViewId: Int,
                              fragment: Fragment,
                              fragmentTag: String) {
        supportFragmentManager
                .beginTransaction()
                .add(containerViewId, fragment, fragmentTag)
                .disallowAddToBackStack()
                .commit()
    }

    protected fun replaceFragment(containerViewId: Int,
                                  fragment: Fragment,
                                  fragmentTag: String,
                                  backStackStateName: String) {
        supportFragmentManager
                .beginTransaction()
                .replace(containerViewId, fragment, fragmentTag)
                .addToBackStack(backStackStateName)
                .commit()
    }

    override fun onBackPressed() {
        ActivityCompat.finishAfterTransition(this)
        //        CommonUtil.handleBackStack(this);
    }

    override fun onDestroy() {
        super.onDestroy()
    }
}