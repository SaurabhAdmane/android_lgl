package com.saad.letsgolive.baseclass

import android.databinding.BaseObservable

import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

/**
 * Created by saurabh on 02/06/2018.
 */

abstract class BaseViewModel : BaseObservable() {

    var dialogVisibility = PublishSubject.create<Boolean>()

    abstract fun subscribe()

    abstract fun clear()

    abstract fun dispose()

    fun getDialogVisibility(): Observable<Boolean> {
        return dialogVisibility.hide()
    }

}
