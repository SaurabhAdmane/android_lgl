package com.saad.letsgolive.network;

import com.fasterxml.jackson.databind.ObjectReader;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Converter;

/**
 * Created by saurabh on 31-05-2018.
 */

public class LGLResponseBodyConverter<T> implements Converter<ResponseBody, T> {

    private final ObjectReader adapter;

    LGLResponseBodyConverter(ObjectReader adapter) {
        this.adapter = adapter;
    }

    @Override
    public T convert(ResponseBody value) throws IOException {
        try {
            return adapter.readValue(value.charStream());
        } finally {
            value.close();
        }
    }
}