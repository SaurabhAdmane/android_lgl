package com.saad.letsgolive.network;

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import com.saad.letsgolive.BuildConfig;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;

/**
 * For REST over HTTP(S). Holds the client for other services to put interfaces against.
 */
public class RestClient {

    public RestClient() {
        getClient();
    }

    /**
     * @return
     */
    public static Retrofit getClient() {
        return RetrofitAPI.retrofit;
    }


    private static class RetrofitAPI {
        private static HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();

        static final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .readTimeout(60, TimeUnit.SECONDS)
//               .certificatePinner(new CertificatePinner.Builder()
//                        .add("api.tmwpay.com", S_KESYL)
//                        .build())
                .addInterceptor(BuildConfig.DEBUG ? httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY) : httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.NONE))
//                .addInterceptor(new ConnectivityInterceptor())
                .retryOnConnectionFailure(true)
                .connectTimeout(60, TimeUnit.SECONDS)
                .build();
        /**
         * For TMW APIs
         */
        private static Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(LGLApi.getBaseUrl())
                .client(okHttpClient)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(LGLConverterFactory.create())
                .build();

    }
}
