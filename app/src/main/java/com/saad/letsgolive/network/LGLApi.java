package com.saad.letsgolive.network;

import com.saad.letsgolive.BuildConfig;

/**
 * Created by Saurabh on 6/20/2018.
 */

public class LGLApi {

    public static String LGL_BASE_URL = "http://23.101.141.185/letsgolive/api/v1";
    public static final String ENDPOINT_PROFILE = "update-profile.php";
    public static final String ENDPOINT_SIGN_UP = "register-user.php";
    public static final String ENDPOINT_LOGIN = "login.php";

    public static String getBaseUrl() {
        return LGL_BASE_URL;
    }

    public static void setAPIEnvironment() {
        switch (APIEnvironment.valueOf(BuildConfig.ENV)) {
            case DEBUG:
//                 setupDevelopmentEnvironment();
                setupDevelopmentEnvironment();
                //   setupProductionEnvironment();
                break;

            case RELEASE:
                //  setupStagingEnvironment();
                setupProductionEnvironment();
                break;

            default:
                setupDevelopmentEnvironment();
                break;
        }
    }

    private static void setupProductionEnvironment() {
//        LGL_BASE_URL = "http://smswalas.co.in/live/";
        LGL_BASE_URL = "http://23.101.141.185/letsgolive/api/v1/";
    }

    private static void setupDevelopmentEnvironment() {
//        LGL_BASE_URL = "http://smswalas.co.in/live/";
        LGL_BASE_URL = "http://23.101.141.185/letsgolive/api/v1/";
    }

    public enum APIEnvironment {
        DEBUG, RELEASE
    }
}
